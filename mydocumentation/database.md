# Posgresql infos

## Creation automatique des identifiants

https://stackoverflow.com/questions/787722/postgresql-autoincrement

Par exemple pour une table compte 

```sql
CREATE SEQUENCE entretien_id_seq;
ALTER TABLE entretien ALTER id_entretien SET DEFAULT NEXTVAL('entretien_id_seq');
```


