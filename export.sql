PGDMP                         w           LocHappy #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1) #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1) '    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16462    LocHappy    DATABASE     |   CREATE DATABASE "LocHappy" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';
    DROP DATABASE "LocHappy";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    13049    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16464    agence_id_seq    SEQUENCE     v   CREATE SEQUENCE public.agence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.agence_id_seq;
       public       postgres    false    3            �            1259    16466    agence    TABLE     �   CREATE TABLE public.agence (
    adresse character(80),
    ville character(80),
    code_postal character(80),
    pays character(80),
    id_agence integer DEFAULT nextval('public.agence_id_seq'::regclass) NOT NULL
);
    DROP TABLE public.agence;
       public         postgres    false    196    3            �            1259    16470    client_id_seq    SEQUENCE     v   CREATE SEQUENCE public.client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.client_id_seq;
       public       postgres    false    3            �            1259    16472    vendeur_id_seq    SEQUENCE     w   CREATE SEQUENCE public.vendeur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.vendeur_id_seq;
       public       postgres    false    3            �            1259    16474    compte    TABLE       CREATE TABLE public.compte (
    id_vendeur integer DEFAULT nextval('public.vendeur_id_seq'::regclass) NOT NULL,
    nom character(80),
    prenom character(80),
    adresse_mail character(80),
    identifiant character(80),
    mot_de_passe character(80),
    id_agence integer
);
    DROP TABLE public.compte;
       public         postgres    false    199    3            �            1259    16515    entretien_id_seq    SEQUENCE     y   CREATE SEQUENCE public.entretien_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.entretien_id_seq;
       public       postgres    false    3            �            1259    16491 	   entretien    TABLE     �   CREATE TABLE public.entretien (
    id_voiture integer,
    id_entretien integer DEFAULT nextval('public.entretien_id_seq'::regclass) NOT NULL,
    date_entree character(80),
    date_sortie character(80)
);
    DROP TABLE public.entretien;
       public         postgres    false    205    3            �            1259    16478    fiche_client    TABLE     �  CREATE TABLE public.fiche_client (
    id_client integer DEFAULT nextval('public.client_id_seq'::regclass) NOT NULL,
    nom character(80),
    prenom character(80),
    date_de_naissance character(80),
    permis character(80),
    num_telephone character(80),
    adresse_mail character(80),
    adresse character(80),
    ville character(80),
    code_postal character(80),
    pays character(80)
);
     DROP TABLE public.fiche_client;
       public         postgres    false    198    3            �            1259    16528    moteur    TABLE     P   CREATE TABLE public.moteur (
    id_moteur integer,
    moteur character(80)
);
    DROP TABLE public.moteur;
       public         postgres    false    3            �            1259    16525 	   puissance    TABLE     Y   CREATE TABLE public.puissance (
    id_puissance integer,
    puissance character(80)
);
    DROP TABLE public.puissance;
       public         postgres    false    3            �            1259    16522    type    TABLE     J   CREATE TABLE public.type (
    id_type integer,
    type character(80)
);
    DROP TABLE public.type;
       public         postgres    false    3            �            1259    16507    voiture_id_seq    SEQUENCE     w   CREATE SEQUENCE public.voiture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.voiture_id_seq;
       public       postgres    false    3            �            1259    16494    voiture    TABLE     �  CREATE TABLE public.voiture (
    id_voiture integer DEFAULT nextval('public.voiture_id_seq'::regclass) NOT NULL,
    marque character(80),
    modele character(80),
    transmission character(80),
    immatriculation character(80),
    nb_km_compteur character(80),
    entretien character(80),
    id_moteur integer,
    id_puissance integer,
    prix integer,
    id_type integer
);
    DROP TABLE public.voiture;
       public         postgres    false    204    3            �          0    16466    agence 
   TABLE DATA               N   COPY public.agence (adresse, ville, code_postal, pays, id_agence) FROM stdin;
    public       postgres    false    197   �(       �          0    16474    compte 
   TABLE DATA               m   COPY public.compte (id_vendeur, nom, prenom, adresse_mail, identifiant, mot_de_passe, id_agence) FROM stdin;
    public       postgres    false    200   w)       �          0    16491 	   entretien 
   TABLE DATA               W   COPY public.entretien (id_voiture, id_entretien, date_entree, date_sortie) FROM stdin;
    public       postgres    false    202   �*       �          0    16478    fiche_client 
   TABLE DATA               �   COPY public.fiche_client (id_client, nom, prenom, date_de_naissance, permis, num_telephone, adresse_mail, adresse, ville, code_postal, pays) FROM stdin;
    public       postgres    false    201   %+       �          0    16528    moteur 
   TABLE DATA               3   COPY public.moteur (id_moteur, moteur) FROM stdin;
    public       postgres    false    208   �+       �          0    16525 	   puissance 
   TABLE DATA               <   COPY public.puissance (id_puissance, puissance) FROM stdin;
    public       postgres    false    207   ,       �          0    16522    type 
   TABLE DATA               -   COPY public.type (id_type, type) FROM stdin;
    public       postgres    false    206   I,       �          0    16494    voiture 
   TABLE DATA               �   COPY public.voiture (id_voiture, marque, modele, transmission, immatriculation, nb_km_compteur, entretien, id_moteur, id_puissance, prix, id_type) FROM stdin;
    public       postgres    false    203   �,       �           0    0    agence_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.agence_id_seq', 4, true);
            public       postgres    false    196            �           0    0    client_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.client_id_seq', 1, true);
            public       postgres    false    198            �           0    0    entretien_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.entretien_id_seq', 13, true);
            public       postgres    false    205            �           0    0    vendeur_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.vendeur_id_seq', 7, true);
            public       postgres    false    199            �           0    0    voiture_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.voiture_id_seq', 40, true);
            public       postgres    false    204                       2606    16486    agence id_agkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.agence
    ADD CONSTRAINT id_agkey PRIMARY KEY (id_agence);
 9   ALTER TABLE ONLY public.agence DROP CONSTRAINT id_agkey;
       public         postgres    false    197                       2606    16488    fiche_client id_clkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.fiche_client
    ADD CONSTRAINT id_clkey PRIMARY KEY (id_client);
 ?   ALTER TABLE ONLY public.fiche_client DROP CONSTRAINT id_clkey;
       public         postgres    false    201                       2606    16514    entretien id_entkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.entretien
    ADD CONSTRAINT id_entkey PRIMARY KEY (id_entretien);
 =   ALTER TABLE ONLY public.entretien DROP CONSTRAINT id_entkey;
       public         postgres    false    202                       2606    16490    compte id_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.compte
    ADD CONSTRAINT id_pkey PRIMARY KEY (id_vendeur);
 8   ALTER TABLE ONLY public.compte DROP CONSTRAINT id_pkey;
       public         postgres    false    200                       2606    16498    voiture id_vokey 
   CONSTRAINT     V   ALTER TABLE ONLY public.voiture
    ADD CONSTRAINT id_vokey PRIMARY KEY (id_voiture);
 :   ALTER TABLE ONLY public.voiture DROP CONSTRAINT id_vokey;
       public         postgres    false    203            �   �   x��ҽ
�0��y�{IM�:Z��(.��R9S�G�s�Ō"�͖��ݟ���T!l�������te��ho��И�#����2�7��\cC�#f���� ���{���W��%��߅�[����f��ZD��u�6�R��+*�&�c�3K��7�{Y�V%Vf�ΥT��^1������      �     x���MN�0���=A��6,R�JUժ��ec��$qb�pn�=�`x7��>���9�cX�f�;TI�%�j��i�D�aX�w��U����Ldts���#2��a\%��:f���5ڃw�`���"�g� ��3�Ll��qND,<!xg:_c0Sds�������Ŏ��ݚL�flJ��ڷ�u����ǡ����l�s�@'�,�'��E:c�/�.'���p����z&��G�?t�k���j����>��3N��յ�=�A��L�K"��{|w�      �   r   x���[� D�(�t�a���fja����	k�]x䠭�@J��T����Ъr03;�u9��⻻�̣�k�52�+t�3�ܗ�ҿ��:Yc�[�V�s3����      �   �   x�3�K-�M��Q��tJ,.�Lͣ���������f�2����܂:��4071�43632���I�0�ƍCF~Inbf�^r~.��)��*��+d�^��WL����2srR)4a�����bb�[Qb^2�ܧ������ o�{s      �   >   x�3�t�L-N�Q��2�t-.N�KN���Ɯ�IE�)T4Ҕ�5'5��(���J�r��qqq ��1�      �   &   x�3�4U�.�2�4���Ɯ��6҄Ә�fr��qqq �-*+      �   �   x�����0��=E'@����p1�BVC9	;1�v�7�#��z�%Q(r�3���e�����f7��;��9���� ���x��5}?^��<B���F.^�	�2�]MN`#�Ls�7�ٯa�jO]����B�}�~1      �   )  x���n�H��ç�̂=?�(Ȗd�ı��5�\&�!̐6)f�<}zH�q����ƹ��f���kƈO��T���B�96w�ʺ.a�Õ46�P�i
bM���$ȑP����
��/��Άs�͟��#���3$�]a�i�)������=_���][�8"^��Q��W�2�ʈH�4�2�ź,_��}ٔO���L�V�ZK��OXF�52���T�	i�j��5ͫ��#/��^�L��
I�bM�qi�l($����bEڴ�ʿ�x�pͦA���Zk���]��"����*(��:��hyM��5
h�L$�\H:!k�4���{���.�f-3m���#��Y&��"K
��l!���D��H���\nn!8f��'R�v�`�M����.�;�u웏���<Z�Q�XK��PF��Ǫ�}|��￾z\m�E�������
��@�	8�o��(�4P���x���Ax��~�2�rI]�X����<����s���a+����9
��-�D"a�~SB�hw�Dh%�ˁ��Y��HA���e���e�o �_v�9]�k1@�F���jDQv$#���C���w-0k��#i���(`�)�E�ɤ����P��,|��ݑ\\Apc�g�5G��L�Y��U����ݟ�c:�[���^s�L�Q��W��03 l�EZo$)+�R3�]^r!BW$n����Z�����x�p��M5�� ��T�U�p	bWv�婄�-qآ�>�����E��6DR&d���KYC�΢��Ox%=L"�um�)��0*7a$|@{��j�0;������v���Ƒ���.�s䚛���G��;�y`hs�ٱF���z���j�9�F���&���x^�S|���gQ�R� nI�?�B�nh�ߙX��	��.|���a@�H����N��,~"�H���������+��i������2�{�Ò�}[���}��z�����*6�3�n&��lI�%��ʞrx�p������c��7�\���t��(�,��ߒ$�	��     