PGDMP     	    +                w        	   LocaHappy #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1) #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16397 	   LocaHappy    DATABASE     }   CREATE DATABASE "LocaHappy" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';
    DROP DATABASE "LocaHappy";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    13049    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16454    agence_id_seq    SEQUENCE     v   CREATE SEQUENCE public.agence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.agence_id_seq;
       public       postgres    false    3            �            1259    16398    agence    TABLE     �   CREATE TABLE public.agence (
    adresse character(80),
    ville character(80),
    code_postal character(80),
    pays character(80),
    id_agence integer DEFAULT nextval('public.agence_id_seq'::regclass) NOT NULL
);
    DROP TABLE public.agence;
       public         postgres    false    200    3            �            1259    16459    client_id_seq    SEQUENCE     v   CREATE SEQUENCE public.client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.client_id_seq;
       public       postgres    false    3            �            1259    16443    vendeur_id_seq    SEQUENCE     w   CREATE SEQUENCE public.vendeur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.vendeur_id_seq;
       public       postgres    false    3            �            1259    16401    compte    TABLE       CREATE TABLE public.compte (
    id_vendeur integer DEFAULT nextval('public.vendeur_id_seq'::regclass) NOT NULL,
    nom character(80),
    prenom character(80),
    adresse_mail character(80),
    identifiant character(80),
    mot_de_passe character(80),
    id_agence integer
);
    DROP TABLE public.compte;
       public         postgres    false    199    3            �            1259    16404    fiche_client    TABLE     �  CREATE TABLE public.fiche_client (
    id_client integer DEFAULT nextval('public.client_id_seq'::regclass) NOT NULL,
    nom character(80),
    prenom character(80),
    date_de_naissance character(80),
    permis character(80),
    num_telephone character(80),
    adresse_mail character(80),
    adresse character(80),
    ville character(80),
    code_postal character(80),
    pays character(80)
);
     DROP TABLE public.fiche_client;
       public         postgres    false    201    3            y          0    16398    agence 
   TABLE DATA               N   COPY public.agence (adresse, ville, code_postal, pays, id_agence) FROM stdin;
    public       postgres    false    196   �       z          0    16401    compte 
   TABLE DATA               m   COPY public.compte (id_vendeur, nom, prenom, adresse_mail, identifiant, mot_de_passe, id_agence) FROM stdin;
    public       postgres    false    197   �       {          0    16404    fiche_client 
   TABLE DATA               �   COPY public.fiche_client (id_client, nom, prenom, date_de_naissance, permis, num_telephone, adresse_mail, adresse, ville, code_postal, pays) FROM stdin;
    public       postgres    false    198   �       �           0    0    agence_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.agence_id_seq', 4, true);
            public       postgres    false    200            �           0    0    client_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.client_id_seq', 1, true);
            public       postgres    false    201            �           0    0    vendeur_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.vendeur_id_seq', 5, true);
            public       postgres    false    199            �
           2606    16447    agence id_agkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.agence
    ADD CONSTRAINT id_agkey PRIMARY KEY (id_agence);
 9   ALTER TABLE ONLY public.agence DROP CONSTRAINT id_agkey;
       public         postgres    false    196            �
           2606    16458    fiche_client id_clkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.fiche_client
    ADD CONSTRAINT id_clkey PRIMARY KEY (id_client);
 ?   ALTER TABLE ONLY public.fiche_client DROP CONSTRAINT id_clkey;
       public         postgres    false    198            �
           2606    16442    compte id_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.compte
    ADD CONSTRAINT id_pkey PRIMARY KEY (id_vendeur);
 8   ALTER TABLE ONLY public.compte DROP CONSTRAINT id_pkey;
       public         postgres    false    197            y   �   x��ҽ
�0��y�{IM�:Z��(.��R9S�G�s�Ō"�͖��ݟ���T!l�������te��ho��И�#����2�7��\cC�#f���� ���{���W��%��߅�[����f��ZD��u�6�R��+*�&�c�3K��7�{Y�V%Vf�ΥT��^1������      z   �   x���M
�0����@�� Z\�v�J���&=S�ы5S{_��&��P�3�Q��Q#�]�Qbì��<�;���p�)LUJ��/�;�(h9i��� j�#f1�;[�����Z����\et������n�Ʉ�x9�HB���\C1k`3���*?|�rء��?��������)��~*�2��9��-VJ}����      {   �   x�3�K-�M��Q��tJ,.�Lͣ���������f�2����܂:��4071�43632���I�0�ƍCF~Inbf�^r~.��)��*��+d�^��WL����2srR)4a�����bb�[Qb^2�ܧ������ o�{s     