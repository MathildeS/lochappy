PGDMP         '                w           LocHappy    11.2    11.2 .    Q           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            R           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            S           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            T           1262    16393    LocHappy    DATABASE     �   CREATE DATABASE "LocHappy" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE "LocHappy";
             postgres    false            �            1259    16394    agence_id_seq    SEQUENCE     v   CREATE SEQUENCE public.agence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.agence_id_seq;
       public       postgres    false            �            1259    16396    agence    TABLE     �   CREATE TABLE public.agence (
    adresse character(80),
    ville character(80),
    code_postal character(80),
    pays character(80),
    id_agence integer DEFAULT nextval('public.agence_id_seq'::regclass) NOT NULL
);
    DROP TABLE public.agence;
       public         postgres    false    196            �            1259    16400    client_id_seq    SEQUENCE     v   CREATE SEQUENCE public.client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.client_id_seq;
       public       postgres    false            �            1259    16402    vendeur_id_seq    SEQUENCE     w   CREATE SEQUENCE public.vendeur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.vendeur_id_seq;
       public       postgres    false            �            1259    16404    compte    TABLE       CREATE TABLE public.compte (
    id_vendeur integer DEFAULT nextval('public.vendeur_id_seq'::regclass) NOT NULL,
    nom character(80),
    prenom character(80),
    adresse_mail character(80),
    identifiant character(80),
    mot_de_passe character(80),
    id_agence integer
);
    DROP TABLE public.compte;
       public         postgres    false    199            �            1259    16408    entretien_id_seq    SEQUENCE     y   CREATE SEQUENCE public.entretien_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.entretien_id_seq;
       public       postgres    false            �            1259    16410 	   entretien    TABLE     �   CREATE TABLE public.entretien (
    id_voiture integer,
    id_entretien integer DEFAULT nextval('public.entretien_id_seq'::regclass) NOT NULL,
    date_entree character(80),
    date_sortie character(80)
);
    DROP TABLE public.entretien;
       public         postgres    false    201            �            1259    16414    fiche_client    TABLE     �  CREATE TABLE public.fiche_client (
    id_client integer DEFAULT nextval('public.client_id_seq'::regclass) NOT NULL,
    nom character(80),
    prenom character(80),
    date_de_naissance character(80),
    permis character(80),
    num_telephone character(80),
    adresse_mail character(80),
    adresse character(80),
    ville character(80),
    code_postal character(80),
    pays character(80)
);
     DROP TABLE public.fiche_client;
       public         postgres    false    198            �            1259    16511    moteur_id_seq    SEQUENCE     v   CREATE SEQUENCE public.moteur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.moteur_id_seq;
       public       postgres    false            �            1259    16506    moteur    TABLE     �   CREATE TABLE public.moteur (
    id_moteur integer DEFAULT nextval('public.moteur_id_seq'::regclass) NOT NULL,
    moteur character(80)
);
    DROP TABLE public.moteur;
       public         postgres    false    211            �            1259    16424 	   puissance    TABLE     Y   CREATE TABLE public.puissance (
    id_puissance integer,
    puissance character(80)
);
    DROP TABLE public.puissance;
       public         postgres    false            �            1259    16501    reservation_id_seq    SEQUENCE     {   CREATE SEQUENCE public.reservation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.reservation_id_seq;
       public       postgres    false            �            1259    16498    reservation    TABLE     (  CREATE TABLE public.reservation (
    id_reservation integer DEFAULT nextval('public.reservation_id_seq'::regclass) NOT NULL,
    date_depart character(80),
    date_retour character(80),
    agence_depart integer,
    agence_retour integer,
    id_voiture integer,
    nb_jours character(80)
);
    DROP TABLE public.reservation;
       public         postgres    false    209            �            1259    16516    type_id_seq    SEQUENCE     t   CREATE SEQUENCE public.type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.type_id_seq;
       public       postgres    false            �            1259    16427    type    TABLE     �   CREATE TABLE public.type (
    id_type integer DEFAULT nextval('public.type_id_seq'::regclass) NOT NULL,
    type character(80)
);
    DROP TABLE public.type;
       public         postgres    false    212            �            1259    16430    voiture_id_seq    SEQUENCE     w   CREATE SEQUENCE public.voiture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.voiture_id_seq;
       public       postgres    false            �            1259    16432    voiture    TABLE     �  CREATE TABLE public.voiture (
    id_voiture integer DEFAULT nextval('public.voiture_id_seq'::regclass) NOT NULL,
    marque character(80),
    modele character(80),
    transmission character(80),
    immatriculation character(80),
    nb_km_compteur character(80),
    entretien character(80),
    id_moteur integer,
    id_puissance integer,
    prix integer,
    id_type integer,
    reserve character(80),
    id_agence integer
);
    DROP TABLE public.voiture;
       public         postgres    false    206            ?          0    16396    agence 
   TABLE DATA               N   COPY public.agence (adresse, ville, code_postal, pays, id_agence) FROM stdin;
    public       postgres    false    197   62       B          0    16404    compte 
   TABLE DATA               m   COPY public.compte (id_vendeur, nom, prenom, adresse_mail, identifiant, mot_de_passe, id_agence) FROM stdin;
    public       postgres    false    200   
3       D          0    16410 	   entretien 
   TABLE DATA               W   COPY public.entretien (id_voiture, id_entretien, date_entree, date_sortie) FROM stdin;
    public       postgres    false    202   64       E          0    16414    fiche_client 
   TABLE DATA               �   COPY public.fiche_client (id_client, nom, prenom, date_de_naissance, permis, num_telephone, adresse_mail, adresse, ville, code_postal, pays) FROM stdin;
    public       postgres    false    203   ~4       L          0    16506    moteur 
   TABLE DATA               3   COPY public.moteur (id_moteur, moteur) FROM stdin;
    public       postgres    false    210   n5       F          0    16424 	   puissance 
   TABLE DATA               <   COPY public.puissance (id_puissance, puissance) FROM stdin;
    public       postgres    false    204   �5       J          0    16498    reservation 
   TABLE DATA               �   COPY public.reservation (id_reservation, date_depart, date_retour, agence_depart, agence_retour, id_voiture, nb_jours) FROM stdin;
    public       postgres    false    208   �5       G          0    16427    type 
   TABLE DATA               -   COPY public.type (id_type, type) FROM stdin;
    public       postgres    false    205   \6       I          0    16432    voiture 
   TABLE DATA               �   COPY public.voiture (id_voiture, marque, modele, transmission, immatriculation, nb_km_compteur, entretien, id_moteur, id_puissance, prix, id_type, reserve, id_agence) FROM stdin;
    public       postgres    false    207   �6       U           0    0    agence_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.agence_id_seq', 4, true);
            public       postgres    false    196            V           0    0    client_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.client_id_seq', 2, true);
            public       postgres    false    198            W           0    0    entretien_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.entretien_id_seq', 13, true);
            public       postgres    false    201            X           0    0    moteur_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.moteur_id_seq', 1, false);
            public       postgres    false    211            Y           0    0    reservation_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.reservation_id_seq', 1, false);
            public       postgres    false    209            Z           0    0    type_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.type_id_seq', 1, false);
            public       postgres    false    212            [           0    0    vendeur_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.vendeur_id_seq', 7, true);
            public       postgres    false    199            \           0    0    voiture_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.voiture_id_seq', 40, true);
            public       postgres    false    206            �
           2606    16437    agence id_agkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.agence
    ADD CONSTRAINT id_agkey PRIMARY KEY (id_agence);
 9   ALTER TABLE ONLY public.agence DROP CONSTRAINT id_agkey;
       public         postgres    false    197            �
           2606    16439    fiche_client id_clkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.fiche_client
    ADD CONSTRAINT id_clkey PRIMARY KEY (id_client);
 ?   ALTER TABLE ONLY public.fiche_client DROP CONSTRAINT id_clkey;
       public         postgres    false    203            �
           2606    16441    entretien id_entkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.entretien
    ADD CONSTRAINT id_entkey PRIMARY KEY (id_entretien);
 =   ALTER TABLE ONLY public.entretien DROP CONSTRAINT id_entkey;
       public         postgres    false    202            �
           2606    16510    moteur id_kmoteur 
   CONSTRAINT     V   ALTER TABLE ONLY public.moteur
    ADD CONSTRAINT id_kmoteur PRIMARY KEY (id_moteur);
 ;   ALTER TABLE ONLY public.moteur DROP CONSTRAINT id_kmoteur;
       public         postgres    false    210            �
           2606    16505    reservation id_kreservation 
   CONSTRAINT     e   ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT id_kreservation PRIMARY KEY (id_reservation);
 E   ALTER TABLE ONLY public.reservation DROP CONSTRAINT id_kreservation;
       public         postgres    false    208            �
           2606    16515    type id_ktype 
   CONSTRAINT     P   ALTER TABLE ONLY public.type
    ADD CONSTRAINT id_ktype PRIMARY KEY (id_type);
 7   ALTER TABLE ONLY public.type DROP CONSTRAINT id_ktype;
       public         postgres    false    205            �
           2606    16443    compte id_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.compte
    ADD CONSTRAINT id_pkey PRIMARY KEY (id_vendeur);
 8   ALTER TABLE ONLY public.compte DROP CONSTRAINT id_pkey;
       public         postgres    false    200            �
           2606    16445    voiture id_vokey 
   CONSTRAINT     V   ALTER TABLE ONLY public.voiture
    ADD CONSTRAINT id_vokey PRIMARY KEY (id_voiture);
 :   ALTER TABLE ONLY public.voiture DROP CONSTRAINT id_vokey;
       public         postgres    false    207            ?   �   x��ҽ
�0��y�{IM�:Z��(.��R9S�G�s�Ō"�͖��ݟ���T!l�������te��ho��И�#����2�7��\cC�#f���� ���{���W��%��߅�[����f��ZD��u�6�R��+*�&�c�3K��7�{Y�V%Vf�ΥT��^1������      B     x���MN�0���=A��6,R�JUժ��ec��$qb�pn�=�`x7��>���9�cX�f�;TI�%�j��i�D�aX�w��U����Ldts���#2��a\%��:f���5ڃw�`���"�g� ��3�Ll��qND,<!xg:_c0Sds�������Ŏ��ݚL�flJ��ڷ�u����ǡ����l�s�@'�,�'��E:c�/�.'���p����z&��G�?t�k���j����>��3N��յ�=�A��L�K"��{|w�      D   8   x����4�44R�*���\F���F��5h u��24�4�41���@gR�@�=... ��?�      E   �   x���A�0�u9EO -ҁ�w&��+7%6��Jċ	�D�2�^&��Kv4�5�q�a۲���@�ERFR��ci�(M�q&�	�Ċ</7|g�_phK��
�A�~4�6=������_�����:gV:P�D%��;_v�~�1+�9kj2��K?X��̯��%K��鱳齁�,J!%P�e�����!^�Ư��*#o�:�kC�IS�):�]9�A� ���      L   >   x�3�L�L-N�Q��2�L-.N�KN���Ɯ�IE�)T4҄35'5��(���J�r��qqq �2u      F   &   x�3�4U�.�2�4���Ɯ��6҄Ә�fr��qqq �-*+      J   Z   x�����0C��L@bS>ݥ3�����vr~���R֞*���l���'���ߦ�0!&�J��7��ΏŜo���.^8@��kD<r)d      G   �   x�����0��=E'@����p1�BVC9	;1�v�7�#��z�%Q(r�3���e�����f7��;��9���� ���x��5}?^��<B���F.^�	�2�]MN`#�Ls�7�ٯa�jO]����B�}�~1      I   ]  x���n�H�ϣ��̂���(Ȗd�ı��5�\&W!���(f�<}�����k���@��5��#��O~�=p�/��W�v_��!�K�$��z�*Vmq80��F{J��tI��X �c�Q&��+�c��Hm^�]DR�T�"�p�s�|���������T.�{��զ8�s	�NO�:�����yX5j��W~��#�Z,u�=@p4�V�P�T*"|P�"�\���}.�C.�r��7z��10���+'E"�gj^G t��EU��g�p���u�r�7L��[����%*�=W���y|O���8��r=�2������7 ����U�����C�4�bb��5��K�z����r�f�Y���u&υ�:�Ԭ�����V>#�����5�N/� 8�M�o�|��rƫe}n$vԜk��HLr���A?�Q@>7��(J�)"��_������[����$g=�u�=��,��է�A��~�}0zr��V��u�g<��^��3|XK	6��W<��Y�kӌ�͝�^I�o 	���o�h�<̝��G��|��������:`�Ѱ{Yu���N���Knb{ �Ǫ��
V�"Qд�&kPj�cM"��:��6
�����O�}�{s�z���)Ǔ��i�f�o����d��Ċ}�p�F����ɐ�;�.`0��΍�}��\�
�ȡ6K�>W ������%����nް^�@l���H��4����Ex��Ҁ��	C慖֨�ꟲ*��R��))v�Op�C�#Ů���2�o�#�8�"?kA��ӯ'���'�1\�����;����,9�z ���m,�,R���x]� �=�������ޝ���9>������_jʂ��B��g����x�[��߲�T�>�'d*@�i�3'�C��C؞�※$��=��vd|kح�m�K9���Ⱦ���l��`�[�	ĺ �Üɤ�_�K��?�Z�7�p�?�����:�Q�o#���#���J|�#u��/���t�bo�op	�/�i2�����/w	���cS?z�W��;��*��z���%����/�ž��}S�G<y|P�M��H�7��? n��,     