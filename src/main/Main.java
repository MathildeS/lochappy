package main;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import action.Application;
import action.RechercheLoc;
import squelette.Client;

/**
 * Classe main
 * @author groupe LocHappy
 *
 */
public class Main {

	private static Scanner scan;

	private static Connection conn;

	/**
	 * Main pour lancer l'application
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException  {
		// LANCER L'APPLICATION
		Application lochappy = new Application("ouverture");
	}

	/**
	 * Getters et Setters
	 */
	public static Scanner getScan() {
		return scan;
	}

	public static void setScan(Scanner scan) {
		Main.scan = scan;
	}

	public static Connection getConn() {
		return conn;
	}

	public static void setConn(Connection conn) {
		Main.conn = conn;
	}
}
