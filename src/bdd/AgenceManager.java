package bdd;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import squelette.Agence;
import squelette.SingletonConnection;

/**
 * Classe pour interagir avec la BDD 
 * @author groupe LocHappy
 *
 */
public class AgenceManager extends AbstractAction{

	Application application;

	/**
	 * Constructeur 1
	 */
	public AgenceManager() {
	}

	/**
	 * Constructeur 2
	 * @param texte
	 * @param appli
	 */
	public AgenceManager(String texte, Application appli) {
		super(texte);
		this.application = appli;
	}
	/**
	 * Methode  creation d'une agence
	 * @return 
	 * @throws SQLException
	 */
	private void saveBDD(Agence agence) throws SQLException  {
		// INSERT INTO agence
		Connection conn = SingletonConnection.getInstance();
		String sql = "INSERT INTO  agence(adresse, ville, code_postal, pays)  VALUES (?,?,?,?)";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setString(1, agence.getAdresse());
		state.setString(2, agence.getVille());
		state.setString(3, agence.getCode_postal());
		state.setString(4, agence.getPays());

		// Execution
		state.execute();
		state.close();
	}

	/**
	 * Methode pour recuperer les donnees d'agence avec seulement l'id_agence
	 * @param id_agence
	 * @return liste des donnees de l'agence correspondant a l'id
	 * @throws SQLException
	 */
	public List<String> recupDonnesAgence(int id_agence) throws SQLException{
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT * \r\n" + 
				" FROM agence AS a  \r\n" + 
				" WHERE a.id_agence = ?" ; 
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, id_agence);
		ResultSet resultat = statement.executeQuery();
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String adresse = resultat.getString("adresse");
			String code_postal = resultat.getString("code_postal");
			String ville = resultat.getString("ville");
			String pays = resultat.getString("pays");

			donne.add(adresse);
			donne.add(code_postal);
			donne.add(ville);
			donne.add(pays);
		}
		return donne;
	}

	public List<Integer> recupIdVoiture(int id_agence) throws SQLException{
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT id_voiture \r\n" + 
				" FROM voiture AS v  \r\n" + 
				" WHERE v.id_agence = ?" ; 
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, id_agence);
		ResultSet resultat = statement.executeQuery();
		List<Integer> donne_id = new ArrayList<Integer>() ;
		while (resultat.next()){
			donne_id.add(resultat.getInt("id_voiture"));
		}
		return donne_id;
	}

	@Override
	/**
	 * Methode herite interaction avec le siege
	 * via le bouton parc automobile de l'entreprise
	 */	
	public void actionPerformed(ActionEvent e) {
		AgenceManager ag = new AgenceManager();
		Connection conn = SingletonConnection.getInstance();
		String sql = "SELECT id_agence FROM agence AS a "; 
		PreparedStatement state;
		List<Integer> list_Idagence = new ArrayList<Integer>();
		try {
			state = conn.prepareStatement(sql);
			ResultSet resultSet = state.executeQuery();
			while(resultSet.next()) {
				list_Idagence.add(resultSet.getInt("id_agence"));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String message = "Repartition des vehicules dans les differentes agences : \n\n";
		for(int i=0; i<list_Idagence.size();i++) {
			int id_agence = list_Idagence.get(i);
			try {
				String sql_nb_voiture = "SELECT count(*) AS nb_voitures FROM voiture AS v WHERE v.id_agence = ?"; 
				PreparedStatement state_nb = conn.prepareStatement(sql_nb_voiture);
				state_nb.setInt(1,id_agence);
				ResultSet resultSet = state_nb.executeQuery();
				int nb_voiture = 0;
				if(resultSet.next()) {
					nb_voiture = resultSet.getInt("nb_voitures"); // Le nombre de voitures reservees dans cette agence
					message+="Agence "+id_agence+" = "+nb_voiture+"\n\n";
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		int repagence = JOptionPane.showConfirmDialog(null,message+"\n\n Voulez-vous modifier la repartition ?","Bilan parc automobile",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (repagence == JOptionPane.OK_OPTION) {
			String agence_retire = JOptionPane.showInputDialog(null, "Dans quelle agence voulez-vous pecher le(s) vehicule(s) ?","Transfert vehicule(s)",JOptionPane.QUESTION_MESSAGE);
			int agence_ret = Integer.parseInt(agence_retire);
			String agence_recois = JOptionPane.showInputDialog(null, "Dans quelle agence voulez-vous placer le(s) vehicule(s) ?","Transfert vehicule(s)",JOptionPane.QUESTION_MESSAGE);
			int agence_rec = Integer.parseInt(agence_recois);
			String nbvehicule = JOptionPane.showInputDialog(null, "Combien de vehicules voulez vous prendre ?","Transfert vehicule(s)",JOptionPane.QUESTION_MESSAGE);
			int nb_vehicule = Integer.parseInt(nbvehicule);	
			while(nb_vehicule!=0) {
				List<Integer> list_idVoiture = null;
				try {
					list_idVoiture = ag.recupIdVoiture(agence_ret);
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
				String sql_v = "UPDATE voiture SET id_agence=? WHERE id_voiture=?";
				PreparedStatement statement;
				try {
					statement = conn.prepareStatement(sql_v);
					statement.setInt(1, agence_rec);
					statement.setInt(2, list_idVoiture.get(nb_vehicule));
					statement.executeUpdate();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				nb_vehicule-=1;
			}
			JOptionPane.showMessageDialog(null,"Le transfert a bien ete opere.","Transfert vehicule(s)",JOptionPane.INFORMATION_MESSAGE);
		}
		else{
			JOptionPane.showMessageDialog(null,"Bilan terminer","Bilan parc automobile",JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
