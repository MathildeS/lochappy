package bdd;

import java.awt.event.ActionEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import squelette.Reservation;
import squelette.SingletonConnection;

/**
 * Classe pour interagir avec la BDD et l'utilisateur
 * @author Mathilde
 *
 */
public class ReservationManager extends AbstractAction{

	private Application application;

	/**
	 * Constructeur 1
	 */
	public ReservationManager() {	
	}

	/**
	 * Constructeur 2
	 * @param texte
	 * @param appli
	 */
	public ReservationManager(String texte, Application appli) {
		super(texte);
		this.application = appli;
	}

	/**
	 * Methode creant une reservation
	 * @param reservation
	 * @param id_voiture
	 * @param id_client
	 * @return id_reservation si celle-ci existe
	 * @throws SQLException
	 */
	public static int save(Reservation reservation,int id_voiture, int id_client) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = "INSERT INTO reservation(id_voiture,date_depart, date_retour, agence_depart, agence_retour,nb_jours,id_client) VALUES (?,?,?,?,?,?,?)";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setInt(1, id_voiture);
		state.setString(2, reservation.getDate_depart());
		state.setString(3, reservation.getDate_retour());
		state.setInt(4, reservation.getAgence_depart());
		state.setInt(5, reservation.getAgence_retour());
		state.setString(6, reservation.getNb_jour());
		state.setInt(7, id_client);
		state.execute();
		state.close();

		String sqlId = "SELECT * FROM reservation AS r WHERE r.id_voiture = ?";
		PreparedStatement statement = conn.prepareStatement(sqlId);
		statement.setInt(1,id_voiture);
		ResultSet resultSet = statement.executeQuery();
		if (resultSet.next()) {
			return resultSet.getInt("id_reservation");
		}
		return -1;
	}

	/**
	 * M�thode de recuperation des donnees sur une reservation
	 * @param id_reservation
	 * @return liste des donnees correspondantes
	 * @throws SQLException
	 */
	public List<String> recupDonnesReservation(int id_reservation) throws SQLException{
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT * \r\n" + 
				" FROM reservation AS r  \r\n" + 
				" WHERE r.id_reservation = ?" ; 
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, id_reservation);
		ResultSet resultat = statement.executeQuery();
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String date_depart = resultat.getString("date_depart");
			String nb_jours = resultat.getString("nb_jours");
			String date_retour = resultat.getString("date_retour");
			String agence_depart = resultat.getString("agence_depart");
			String agence_retour = resultat.getString("agence_retour");
			String id_voiture = resultat.getString("id_voiture");
			String id_client = resultat.getString("id_client");

			donne.add(date_depart);
			donne.add(nb_jours);
			donne.add(date_retour);
			donne.add(agence_depart);
			donne.add(agence_retour);
			donne.add(id_voiture);
			donne.add(id_client);
		}
		return donne;
	}

	/**
	 * Methode rendant indisponible un vehicule lou�
	 * @param id_voiture
	 * @throws SQLException
	 */
	public void rendreNonDispo(int id_voiture) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql =  "UPDATE voiture SET reserve='non dispo' WHERE id_voiture= ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, id_voiture);
		statement.executeUpdate();
	}

	@Override
	/**
	 * Methode herite d'interaction avec le siege
	 * donne acces aux données sur une reservation
	 * via le bouton consulter reservation
	 */
	public void actionPerformed(ActionEvent e) {
		String num_reservation = JOptionPane.showInputDialog(null, "Numero de reservation","Consulter une reservation",JOptionPane.QUESTION_MESSAGE);

		int id_reservation = Integer.parseInt(num_reservation);
		ReservationManager resman = new ReservationManager();
		List<String> donnee_r = null;
		try {
			donnee_r = resman.recupDonnesReservation(id_reservation);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		int id_client = Integer.parseInt(donnee_r.get(6));

		ClientManager clientmana = new ClientManager();
		List<String> donnee_c = null;
		try {
			donnee_c = clientmana.recupDonnesClient(id_client);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String nom_client = donnee_c.get(7).replaceAll("\\s", "");;
		String prenom_client = donnee_c.get(8).replaceAll("\\s", "");;
		int id_voiture = Integer.parseInt(donnee_r.get(5));

		VoitureManager voituremana = new VoitureManager();
		List<String> donnee_v = null;
		try {
			donnee_v = voituremana.recupDonneesVoiture(id_voiture);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String marque_voiture = donnee_v.get(1).replaceAll("\\s", "");
		String modele_voiture = donnee_v.get(2).replaceAll("\\s", "");
		String immatriculation = donnee_v.get(0).replaceAll("\\s", "");

		// MESSAGE BILAN
		String message = "Numero de reservation : "+ id_reservation +"\n\n"+
				"Location d'une "+ marque_voiture + "  "+ modele_voiture+"  immatricule  "+
				immatriculation +"\n\nPar  "+nom_client+"  "+ prenom_client + "  Numero client : "+
				id_client +"\n\nDate de depart : " + donnee_r.get(0).replaceAll("\\s", "")+"     "+"Date de retour : "+
				donnee_r.get(2).replaceAll("\\s", "")+"\n\nAgence de depart : " + donnee_r.get(3).replaceAll("\\s", "")+"     "+
				"Agence de retour : "+ donnee_r.get(4).replaceAll("\\s", "");

		JOptionPane.showMessageDialog(null, message,"Consultation de l'agence",JOptionPane.INFORMATION_MESSAGE);
	}
}
