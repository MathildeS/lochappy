package bdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import parc_auto.Voiture;
import squelette.SingletonConnection;

/**
 * Classe qui s'occupe des voitures en base
 * - ajout
 * - recherche
 * - suppression
 * @author groupe LocHappy
 *
 */
public class VoitureManager {

	/**
	 * Constructeur
	 */
	public VoitureManager() {
	}

	/**
	 * Methode qui r�cup�re l'identifiant du v�hicule en entr�e
	 * @param voiture
	 * @return une valeur exub�rante si la voiture n'existe pas et l'id_voiture si elle existe
	 * @throws SQLException
	 */
	public static int bringBackId(String immat) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = "SELECT id_voiture FROM voiture WHERE immatriculation = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, immat);
		ResultSet result = statement.executeQuery();
		System.out.println(statement.toString());
		if (result.next()) {
			return result.getInt("id_voiture");
		}
		return -1;
	}

	/**
	 * Methode qui sauvegarde une voiture dans la BDD via l'objet voiture
	 * @param voiture
	 * @return rien
	 * @throws SQLException
	 */
	public static int save(Voiture voiture) throws SQLException {
		// INSERT INTO entretien
		Connection conn = SingletonConnection.getInstance();

		String sqlV = "INSERT INTO  voiture( marque, modele, transmission, immatriculation, nb_km_compteur)  VALUES (?,?,?,?,?)";
		PreparedStatement stateV = conn.prepareStatement(sqlV);
		stateV.setString(1, voiture.getMarque());
		stateV.setString(2, voiture.getModele());
		stateV.setString(3, voiture.getTransmission());
		stateV.setString(4, voiture.getImmatriculation());
		stateV.setFloat(5, voiture.getNb_km_compteur());
		
		stateV.execute();
		stateV.close();

		// Requ�te sql pour r�cup�rer id de la voiture cr�e afin de cr�er ou non un entretien
		String sql = "SELECT v.id_voiture \n FROM voiture AS v WHERE v.immatriculation = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, voiture.getImmatriculation());
		ResultSet resultat = statement.executeQuery();
		System.out.println(statement.toString());
		// On r�cup�re id_voiture
		int id_voiture = 0;
		if (resultat.next()) {
			id_voiture = resultat.getInt("id_voiture");
		}
		
		// On r�cup�re id_entretien
		int idEntretien = 0;
		if (voiture.getEntretien() != null) {
			idEntretien = EntretienManager.save(voiture.getEntretien(),id_voiture);
		}
		return 0;
	}

	/**
	 * Methode pour recuperer les donnees de la BDD d'une voiture
	 * @param id_voiture
	 * @return liste des donnees d'une voiture grace a son identifiant
	 * @throws SQLException
	 */
	public List<String> recupDonneesVoiture(int id_voiture) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		
		String sql = " SELECT * \r\n" + 
				" FROM voiture AS v  \r\n" + 
				" WHERE v.id_voiture= ?" ; 
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, id_voiture);
		ResultSet resultat = statement.executeQuery();
		
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String immatriculation = resultat.getString("immatriculation");
			String marque = resultat.getString("marque");
			String modele = resultat.getString("modele");
			String transmission = resultat.getString("transmission");
			String prix = resultat.getString("prix");
			String id_type = resultat.getString("id_type");
			String id_puissance = resultat.getString("id_puissance");
			String id_moteur = resultat.getString("id_moteur");
			String reserv = resultat.getString("reserve");
			String entretien = resultat.getString("entretien");

			donne.add(immatriculation);
			donne.add(marque);
			donne.add(modele);
			donne.add(transmission);
			donne.add(prix);
			donne.add(id_type);
			donne.add(id_puissance);
			donne.add(id_moteur);
			donne.add(reserv);
			donne.add(entretien);
		}
		return donne;
	}
}
