package squelette;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import action.Application;
import bdd.ClientManager;
import bdd.ReservationManager;
import bdd.VoitureManager;
import parc_auto.Moteur;
import parc_auto.Puissance;
import parc_auto.Type;
import parc_auto.Voiture;
import squelette.Reservation;

import com.itextpdf.text.Document;

public class Facture extends AbstractAction{

	/**
	 * Attributs du contrat
	 */

	private int id_contrat;
	private float montant_acompte;
	private String nom_client;
	private int num_reservation;
	private static int idagence;

	private static float surplus;
	private static int idvoiture;

	private Application application;

	/**
	 * Pour se connecter a l'interface
	 */
	private String connect;


	/**
	 * Constructeur Contrat
	 * @param id_contrat
	 * @param montant_acompte
	 * @param nom_client
	 * @param num_reservation
	 */
	public Facture(int id_contrat, float montant_acompte, String nom_client, int num_reservation) {

		this.id_contrat = id_contrat;
		this.montant_acompte = montant_acompte;
		this.nom_client = nom_client;
		this.num_reservation = num_reservation;
	}

	/**
	 * Constructeur vide de Contrat
	 */
	public Facture() {
	}

	public Facture(String texte, Application application2) {
		super(texte);
		this.application = application2;
	}

	/**
	 * Methode
	 * @return
	 * @throws SQLException 
	 */
	public float prixfinalPostUtilisation(int id_reservation) throws SQLException {
		float prixFinalPost = 0; 
		// Je recupere le prix final 
		Connection conn = SingletonConnection.getInstance();
		String sql = "SELECT r.id_voiture, r.nb_jours FROM reservation AS r WHERE r.id_reservation=?";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setInt(1, id_reservation);
		ResultSet resultat1 = state.executeQuery();
		int id_voiture = 0;
		int nb_jour = 0;
		if (resultat1.next()){
			id_voiture = resultat1.getInt("id_voiture");
			nb_jour = resultat1.getInt("nb_jours");
		}
		String sql1 = "SELECT v.prix, v.id_type, v.id_moteur, v.id_puissance FROM voiture AS v WHERE v.id_voiture =?";
		PreparedStatement state1 = conn.prepareStatement(sql1);
		state1.setInt(1, id_voiture);
		ResultSet resultat = state1.executeQuery();
		Voiture voiture = new Voiture();
		Reservation reservation = new Reservation(id_voiture);
		int id_type = 0;
		int id_moteur = 0;
		int id_puissance = 0;
		int prix = 0;
		while (resultat.next()) {
			prix = resultat.getInt("prix");
			id_type = resultat.getInt("id_type");
			id_moteur = resultat.getInt("id_moteur");
			id_puissance = resultat.getInt("id_puissance");
		}
		Type type = new Type(id_type);
		Moteur moteur = new Moteur(id_moteur);
		Puissance puissance = new Puissance(id_puissance);
		float prix_final = reservation.prixFinal(prix,id_voiture,puissance,type,moteur,nb_jour);

		// Maintenant que j'ai le prix final je lui ajoute eventuellement les taxes des kilometres en surplus	
		int retourkmEnPlus = JOptionPane.showConfirmDialog(null, "Avez-vous depasse votre forfait kilometrique ? "," Une derniere question ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if( retourkmEnPlus == JOptionPane.OK_OPTION) { // L'entier correspondant a l'indice du bouton clique vaut 0 (donc Oui)
			String reponse = JOptionPane.showInputDialog(null, "De combien ?","Ultime question",JOptionPane.QUESTION_MESSAGE); 
			int reponses= Integer.parseInt(reponse); 
			surplus = reponses/2;
			prixFinalPost = prix_final + surplus;
		} else // Si le client a respecte son forfait kilometrique
		{
			prixFinalPost = prix_final;
		}
		return prixFinalPost;
	}
	public float prixFinalFinal(int id_reservation) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql2 = "SELECT r.agence_retour FROM reservation AS r WHERE r.id_reservation=?";
		PreparedStatement state2 = conn.prepareStatement(sql2);
		ResultSet resultat2 = state2.executeQuery();
		int agenceret =Integer.parseInt("resultat2");
		float prixfinalfinal = prixfinalPostUtilisation(id_reservation);
		String agence_retour = JOptionPane.showInputDialog( "Dans quelle agence retournez-vous votre vehicule ? \n Exemple: 1, 2, 3 ou 4"); 
		int agenceretour = Integer.parseInt(agence_retour); 
		if(agenceretour != agenceret) {
			prixfinalfinal = prixfinalfinal;
		} else {
			prixfinalfinal += 25; 
		}
		return prixfinalfinal;
	}

	public void actionPerformed(ActionEvent arg0) {
		String nom = JOptionPane.showInputDialog(null, "Nom du client","Facture",JOptionPane.QUESTION_MESSAGE);
		String prenom = JOptionPane.showInputDialog(null, "Prenom du client","Facture",JOptionPane.QUESTION_MESSAGE);
		String id_reservation = JOptionPane.showInputDialog(null, "Numero de reservation","Facture",JOptionPane.QUESTION_MESSAGE);
		int idreservation = Integer.parseInt(id_reservation);
		// Code de cr�ation du fichier Pdf

		Document document = new Document();
		try {
			PdfWriter.getInstance((com.itextpdf.text.Document) document, new FileOutputStream(lien));
			((com.itextpdf.text.Document) document).open();

			// J'ajoute le contenu de mon Pdf Contrat
			document.add(new Paragraph("Votre contrat de location\n\n"));
			try {
				document.add(premierTableau(nom, prenom,idreservation));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		catch (DocumentException | FileNotFoundException DE)
		{
			DE.printStackTrace();
		}
		int retourContrat = JOptionPane.showConfirmDialog(null, "Veuillez cliquer sur 'Oui' afin de signer le contrat"," Signature ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

		if( retourContrat == JOptionPane.OK_OPTION) { // L'entier correspondant a l'indice du bouton clique vaut 0 (donc Oui)
			JOptionPane.showMessageDialog(null, "Merci de votre confiance, a bientot sur LocHappy !", "C'est fini !",JOptionPane.INFORMATION_MESSAGE);
		}
		document.close();

		Connection conn = SingletonConnection.getInstance();
		String sql_v = "UPDATE voiture SET reserve='dispo', id_agence = ? WHERE id_voiture=?";
		PreparedStatement statement;
		try {
			statement = conn.prepareStatement(sql_v);
			statement.setInt(1, idagence);
			statement.setInt(2, idvoiture);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String sql_a = "DELETE FROM reservation WHERE id_voiture=?";
		PreparedStatement state;
		try {
			state = conn.prepareStatement(sql_a);
			state.setInt(1, idvoiture);
			state.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	//	// Le lien ou je depose le contrat en Pdf
	//	public static final String lien ="/home/formation/Documents/LocHappy1/Contrat.pdf"; 

	// Le lien ou je depose le contrat en Pdf
	public static final String lien ="C:/Users/msi71/Documents/work/Contrat.pdf"; 

	//Metode permettant de mettre en page mon tableau qui recupere les donnees client et reservation
	public static PdfPTable premierTableau(String nom, String prenom,int idreservation) throws SQLException{
		Facture contrat = new Facture();
		float prixFinalPost = 0;
		float surcoutAgret = 0;
		try {
			prixFinalPost = contrat.prixfinalPostUtilisation(idreservation);
			surcoutAgret = contrat.prixFinalFinal(idreservation);
		} catch (SQLException e1) {
			System.err.println("Il n'y a pas de surcout");
		}
		int montant_acompte = 100;


		float prixPre = prixFinalPost - surplus;
		ClientManager client = new ClientManager();
		List<String> fiche_client=null;
		try {
			fiche_client = client.recupDonneClient(nom, prenom);
		} catch (SQLException e1) {
			System.err.println("On ne peut pas recuperer les donnees");
		}
		String num_telephone = fiche_client.get(1).replaceAll("\\s", "");
		String adresse_mail = fiche_client.get(2).replaceAll("\\s", "");
		String permi = fiche_client.get(8).replaceAll("\\s","");

		ReservationManager reserv = new ReservationManager();
		List<String> reservation=null;
		try {
			reservation = reserv.recupDonnesReservation(idreservation);
		} catch (SQLException e1) {
			System.err.println("On ne peut pas recuperer les donnees");
		}
		String id_voiture = reservation.get(5);
		String agence_depart = reservation.get(3).replaceAll("\\s", "");
		String agence_retour =  reservation.get(4).replaceAll("\\s", "");
		idagence = Integer.parseInt(agence_retour);
		String datedepart = reservation.get(0).replaceAll("\\s", "");
		String dateretour = reservation.get(2).replaceAll("\\s", "");

		idvoiture = Integer.parseInt(id_voiture);
		VoitureManager voituremanag = new VoitureManager();
		List<String> voiture=null;
		try {
			voiture = voituremanag.recupDonneesVoiture(idvoiture);
		} catch (SQLException e1) {
			System.err.println("On ne peut pas recuperer les donnees");
		}
		String immatriculation = voiture.get(0).replaceAll("\\s", "");
		String prix = voiture.get(4).replaceAll("\\s", "");

		if (agence_depart.equals(agence_retour)) {
			//On cr�er un objet table dans lequel on intialise sa taille

			PdfPTable table = new PdfPTable(4);
			//On creer l'objet cellule
			PdfPCell cellule;
			cellule = new PdfPCell(new Phrase("Votre reservation"));
			cellule.setColspan(3);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("N de reservation : " + idreservation));
			cellule.setColspan(1);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase(nom+"  "+prenom +"\n\nN permis : "+permi));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase(" Coordonnees : \n\nN tel. : "+num_telephone+"\n\nAdresse mail : "+adresse_mail));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("Vehicule  N "+id_voiture+"\n\nImmatriculation : "+ immatriculation +
					"\n\nPrix (/jour) "+prix+ " euros"));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("Date de d�but : "+ datedepart +"\n\nAgence d�part : "+agence_depart+
					"\n\nDate de retour : "+ dateretour + "\n\nAgence retour : "+ agence_retour
					+"\n\n(1)Supplemment retour dans une autre agence : + 0 euros"+
					"\n\n(2)Supplemment km(s) : + 0.50 euros/Km "));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("Prix acompte : "+montant_acompte+" euros"+
					"\n\nPrix reservation (incluant (1)) : "+ prixPre+" euros" +
					"\n\nSurplus kilometrique(2) : +"+surplus+" euros"+
					"\n\nPRIX TOTAL LOCATION : "+prixFinalPost+" euros"));
			cellule.setColspan(4);
			table.addCell(cellule);
			return table;  
		}
		else {
			//On creer un objet table dans lequel on intialise sa taille
			PdfPTable table = new PdfPTable(4);
			//On creer l'objet cellule
			PdfPCell cellule;
			cellule = new PdfPCell(new Phrase("Votre reservation"));
			cellule.setColspan(3);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("Num  " + idreservation));
			cellule.setColspan(1);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase(nom+"  "+prenom +"\n\nN permis : "+permi));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase(" Coordonn�es : \n\nN tel. : "+num_telephone+"\n\nAdresse mail : "+adresse_mail));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("Vehicule  N "+id_voiture+"\n\nImmatriculation : "+ immatriculation +
					"\n\nPrix (/jour) "+prix+ " euros"));
			cellule.setColspan(2);
			table.addCell(cellule);

			cellule = new PdfPCell(new Phrase("Date de d�but : "+ datedepart +"\n\nAgence d�part : "+agence_depart+
					"\n\nDate de retour : "+ dateretour + "\n\nAgence retour : "+ agence_retour
					+"\n\n(1)Supplemment retour dans une autre agence : + 0 euros"+
					"\n\n(2)Supplemment km(s) : + 0.50 euros/Km "));
			cellule.setColspan(2);
			table.addCell(cellule);
			cellule = new PdfPCell(new Phrase("Prix acompte : "+montant_acompte+" euros" +
					"\n\nPrix reservation (incluant (1)) : "+ prixPre+" euros" +
					"\n\nSurplus kilometrique(2) : +"+surplus+" euros"+
					"\n\nPRIX TOTAL LOCATION : "+prixFinalPost+" euros"));
			cellule.setColspan(4);
			table.addCell(cellule);
			return table;      
		}
	}
}

//https://www.supinfo.com/articles/single/1187-creer-pdf-avec-itext
