package squelette;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import action.RechercheLoc;

/**
<<<<<<< HEAD
 * classe client afin de :
 * 	- creer une fiche client
=======
 * Classe client afin de :
 * 	- cr�er une fiche client
>>>>>>> 2baa6da3c7bd93be9d7cc70059e41f81f99cdeef
 * 	- enregistrer une fiche client
 * @author groupe LocHappy
 *
 */
public class Client {

	/**
	 * Attributs du client
	 */
	private int id_client;
	private String nom;
	private String prenom;
	private String date_de_naissance;
	private String permis;
	private String num_telephone;
	private String adresse_mail;
	private String adresse;
	private String ville;
	private String code_postal;
	private String pays;

	private String[] anneep = {"2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2033","2034","2035","2036"};

	/**
	 * Connexion
	 */

	private static String connect;// = null;

	public Application application;
	public String text;

	/**
	 * Constructeur 1
	 * @param texte
	 * @param application
	 */
	public Client(String texte, Application application) {
		this.text = texte;
		this.application = application;
	}

	/**
	 * Constructeur 2
	 * @param nom
	 * @param prenom
	 * @param date_de_naissance
	 * @param permis
	 * @param num_telephone
	 * @param adresse_mail
	 * @param adresse
	 * @param ville
	 * @param code_postal
	 * @param pays
	 */
	public Client(String nom, String prenom, String date_de_naissance, String permis, String num_telephone,String adresse_mail, String adresse, String ville, String code_postal, String pays ) {
		this.nom = nom;
		this.prenom = prenom;
		this.date_de_naissance = date_de_naissance;
		this.permis = permis; // seulement le numéros de permis
		this.num_telephone = num_telephone;
		this.adresse_mail = adresse_mail;
		this.adresse = adresse;
		this.ville = ville;
		this.code_postal = code_postal;
		this.pays = pays;
	}

	public Client(int id_client) {
		this.id_client = id_client;
	}

	public Client(int id_client2, String nom, String prenom, String date_de_naissance, String permis, String num_telephone,
			String adresse_mail, String adresse, String ville, String code_postal, String pays) {
		this.id_client = id_client2;
		this.nom = nom;
		this.prenom = prenom;
		this.date_de_naissance = date_de_naissance;
		this.permis = permis; // seulement le numéros de permis
		this.num_telephone = num_telephone;
		this.adresse_mail = adresse_mail;
		this.adresse = adresse;
		this.ville = ville;
		this.code_postal = code_postal;
		this.pays = pays;
	}

	/**
	 * Setters
	 */
	public void setNom (String nom) {
		this.nom = nom;
	}

	public void setPrenom (String prenom) {
		this.prenom = prenom;
	}

	public void setDate (String date_de_naissance) {
		this.date_de_naissance = date_de_naissance;
	}

	public void setPermis (String permis) {
		this.permis = permis;
	}

	public void setNtel (String num) {
		this.num_telephone = num;
	}

	public void setMail (String mail) {
		this.adresse_mail = mail;
	}

	public void setAdresse (String adresse) {
		this.adresse = adresse;
	}

	public void setCodeP (String cp) {
		this.code_postal = cp;
	}

	public void setVille (String ville) {
		this.ville = ville;
	}

	public void setPays (String pays) {
		this.pays = pays;

	}

	/**
	 * Getters
	 */
	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public int getId_client() {
		return this.id_client;
	}

	/**
	 * M�thode pour v�rifier le permis du client
	 * @param permis
	 * @return un bool�en
	 */
	public boolean  verification_permis(String permis) {
		RechercheLoc rec = new RechercheLoc();
		Boolean soucis = false;
		int j = 0;
		while (j<3 && soucis.equals(false)) {
			j++;
			if(j==3) {
				break;
			}
			if(permis.length()!=9) {
				JOptionPane.showMessageDialog(null,  "Ce n'est pas le bon format, retentez !", "Client", JOptionPane.INFORMATION_MESSAGE);
				permis = JOptionPane.showInputDialog(null,"N� permis","Client",JOptionPane.QUESTION_MESSAGE);
				soucis = false;
			}
			else { 
				JOptionPane.showMessageDialog(null,  "Le numero de permis est valide.", "Client", JOptionPane.INFORMATION_MESSAGE);
				String moisp = (String)JOptionPane.showInputDialog(null, "Entrez votre mois d'expiration","Client",JOptionPane.QUESTION_MESSAGE,null,rec.getMois(),rec.getMois()[0]);
				String anneep = (String)JOptionPane.showInputDialog(null, "Entrez votre annee d'expiration","Client",JOptionPane.QUESTION_MESSAGE,null,this.anneep,this.anneep[0]);
				int an = Integer.parseInt(anneep);
				System.out.println("Nous sommes dans le else");
				if (an>2019) {
					JOptionPane.showMessageDialog(null,  "Le permis est valide.", "Client", JOptionPane.INFORMATION_MESSAGE);
					soucis = true;
					System.out.println("Break");
					break;
				} else {
					String reponse = JOptionPane.showInputDialog(null,  "Le permis a expire. Retenter ?", "Client", JOptionPane.QUESTION_MESSAGE);
					if (reponse.equals("oui")) {
						moisp = (String)JOptionPane.showInputDialog(null, "Entrez votre mois d'expiration","Client",JOptionPane.QUESTION_MESSAGE,null,rec.getMois(),rec.getMois()[0]);
						anneep = (String)JOptionPane.showInputDialog(null, "Entrez votre annee d'expiration","Client",JOptionPane.QUESTION_MESSAGE,null,this.anneep,this.anneep[0]);
						// Hypothese : demande une seconde fois
					}
					else {
						soucis = false;
						break;
					}
				}
			}
		}
		return soucis;
	}
	/**
	 * M�thode pour enregistrer les donn�es clients dans une BDD
	 * @throws SQLException
	 */

	public void saveBDD() throws SQLException  {

		// INSERT INTO Etudiant
		Connection conn = SingletonConnection.getInstance();
		String sql = "INSERT INTO  fiche_client(nom, prenom, date_de_naissance,  permis,  num_telephone, adresse_mail,  adresse,  ville,  code_postal,  pays)  VALUES (?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setString(1, this.nom);
		state.setString(2, this.prenom);
		state.setString(3, this.date_de_naissance);
		state.setString(4, this.permis);
		state.setString(5, this.num_telephone);
		state.setString(6, this.adresse_mail);
		state.setString(7, this.adresse);
		state.setString(8, this.ville);
		state.setString(9, this.code_postal);
		state.setString(10, this.pays);

		//execution
		state.execute();
		state.close();
	}

	/**
	 * Methode de creation d'une fiche client
	 * @param e
	 */
	public void fiche_client(ActionEvent e) {
		String nom = JOptionPane.showInputDialog(null, "Nom","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String prenom = JOptionPane.showInputDialog(null, "Prenom","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String date_de_naissance = JOptionPane.showInputDialog(null, "Date de naissance","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String permis = JOptionPane.showInputDialog(null, "Numero de permis","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String num_telephone = JOptionPane.showInputDialog(null, "Numero de telephone","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String mail = JOptionPane.showInputDialog(null, "Adresse Mail","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String adresse = JOptionPane.showInputDialog(null, "Adresse","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String ville = JOptionPane.showInputDialog(null, "Ville","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String code_postal = JOptionPane.showInputDialog(null, "Code postal","Creation fiche client",JOptionPane.QUESTION_MESSAGE);
		String pays = JOptionPane.showInputDialog(null, "Pays","Creation fiche client",JOptionPane.QUESTION_MESSAGE);

		Client fiche = new Client(nom, prenom, date_de_naissance, permis, num_telephone,mail,adresse,ville, code_postal,pays);

		try {
			fiche.saveBDD();
			JOptionPane.showMessageDialog(null,  "La fiche a bien ete cree", "Creation fiche client", JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
}
