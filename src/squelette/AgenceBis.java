package squelette;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JOptionPane;

import action.Application;
import bdd.AgenceManager;

/**
 * Classe AgenceBis pour pouvoir créer le bouton "Consulter agence"
 * @author formation
 *
 */
public class AgenceBis extends Agence{

	public AgenceBis(String texte, Application appli) {
		super(texte, appli);
	}
	
	@Override
	/**
	 * Methode pour consulter des agences
	 */
	public void actionPerformed(ActionEvent e) {
		String nomagence = JOptionPane.showInputDialog(null, "Nom de l'agence","Consulter Agence",JOptionPane.QUESTION_MESSAGE);
		// On recupere l'adresse de l'agence
		AgenceManager agencemanag = new AgenceManager();
		
		
		List<String> donnees = null;
		int id_agence = Integer.parseInt(nomagence);
		Agence agence = new Agence(id_agence);
		try {
			donnees = agencemanag.recupDonnesAgence(id_agence);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		// On recupere le parc immobilier de l'agence
		Connection conn = SingletonConnection.getInstance();
		int nb_voitures = 0;
		String sql = "SELECT count(*) FROM voiture AS v WHERE v.id_agence = ?"; 
		PreparedStatement state;
		try {
			state = conn.prepareStatement(sql);
			state.setInt(1,id_agence);
			ResultSet resultSet = state.executeQuery();
			resultSet.next();
			nb_voitures = resultSet.getInt("count"); 
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String message_v = "" ;
		String sqlVoitures = "SELECT v.marque, v.modele, v.entretien,v.reserve FROM voiture AS v WHERE v.id_agence = ?";
		PreparedStatement stateVoitures;
		try {
			stateVoitures = conn.prepareStatement(sqlVoitures);
			stateVoitures.setInt(1, id_agence);
			ResultSet resultSetVoitures = stateVoitures.executeQuery();
			while (resultSetVoitures.next()) {
				String marque = resultSetVoitures.getString("marque");
				String modele = resultSetVoitures.getString("modele");
				String entret = resultSetVoitures.getString("entretien");
				String loue = resultSetVoitures.getString("reserve");
				
				entret = entret.replaceAll("\\s", "");
				marque = marque.replaceAll("\\s", "");
				modele = modele.replaceAll("\\s", "");
				message_v += marque +"       "+ modele+"      entretien : "+entret+"     location : "+loue+"\n";
		}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String code_postal = donnees.get(1).replaceAll("\\s", "");
		String ville = donnees.get(2).replaceAll("\\s", "");
		String pays = donnees.get(3).replaceAll("\\s", "") ;
		String message = "Agence num "+id_agence+" \n\n Adresse : "+
				donnees.get(0) +"   "+ code_postal +"   "+
		ville +"   "+ pays +"\n*****************	 \n"+
		"Le parc automobile contient  "+ nb_voitures + "  voiture(s) et est compose de \n"
		+ message_v;
		JOptionPane.showMessageDialog(null, message,"Consultation de l'agence",JOptionPane.INFORMATION_MESSAGE);
	}
}
