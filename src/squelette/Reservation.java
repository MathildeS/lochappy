package squelette;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import action.RechercheLoc;
import action.RechercheVoiture;
import bdd.ClientManager;
import bdd.ReservationManager;
import bdd.VoitureManager;
import parc_auto.Moteur;
import parc_auto.Puissance;
import parc_auto.Type;
import parc_auto.Voiture;

public class Reservation  extends AbstractAction {

	/**
	 * Attributs de la reservation
	 */

	public static int id_reservation;

	private String date_depart;

	private String nb_jour;

	public String getNb_jour() {
		return nb_jour;
	}
	private String date_retour;

	private int agence_depart;

	private int agence_retour;

	private int id_voiture;

	private String mois_end_30 = "04060911";

	private String mois_end_31 = "01030507081012";

	private Application application;

	public static int agencedep;
	public static int agenceret;
	/**
	 * Constructeur 1
	 * @param id_reservation
	 * @param date_depart
	 * @param nb_jour
	 * @param date_retour
	 * @param agence_depart
	 * @param agence_retour
	 */
	public Reservation(int id_reservation, String date_depart, String nb_jour, String date_retour,int agence_depart, int agence_retour) {

		this.id_reservation = id_reservation;
		this.date_depart = date_depart;
		this.nb_jour = nb_jour;
		this.date_retour = date_retour;
		this.agence_depart = agence_depart;
		this.agence_retour = agence_retour;
	}

	/**
	 * Constructeur 2
	 * @param texte
	 * @param application2
	 */
	public Reservation(String texte, Application application2) {
		super(texte);
		this.application = application2;
	}

	/**
	 * Constructeur3
	 * @param id_voiture
	 */
	public Reservation(int id_voiture) {
		this.id_voiture = id_voiture;
	}

	/**
	 * Constructeur 4
	 */
	public Reservation() {
	}

	/**
	 * Constructeur 5
	 * @param date_depart
	 * @param nb_jour
	 * @param date_retour
	 * @param agence_depart
	 * @param agence_retour
	 */
	public Reservation( String date_depart, String nb_jour, String date_retour,int agence_depart, int agence_retour) {
		this.date_depart = date_depart;
		this.nb_jour = nb_jour;
		this.date_retour = date_retour;
		this.agence_depart = agence_depart;
		this.agence_retour = agence_retour;
	}

	/**
	 * Getters
	 */
	public int getAgence_depart() {
		return agence_depart;
	}

	public int getAgence_retour() {
		return agence_retour;
	}

	/**
	 * Methode de calcul du prix fixe de la location
	 * @param prix
	 * @param nb_jour
	 * @return
	 */
	public float prixFixeLoc(int prix, int nb_jour) {
		float nb_jour_loc = (float) nb_jour;
		//float nb_jour_loc = Float.parseFloat(nb_jour);
		float prixFixeL = prix*nb_jour_loc;
		return prixFixeL;
	}

	/**
	 * Methode de calcul du prix final de la location
	 * @param prix
	 * @param id_voiture
	 * @param puissance
	 * @param type
	 * @param moteur
	 * @param nb_jour
	 * @return
	 * @throws SQLException
	 */
	public float prixFinal(int prix,int id_voiture, Puissance puissance, Type type, Moteur moteur, int nb_jour) throws SQLException {
		Reservation reser = new Reservation();
		//Taxes pour la puissance
		float a = puissance.prixPlusTaux(id_voiture, prix);
		//Taxes pour le type
		float b = type.prixPlusTaux(id_voiture, prix);
		//Taxes pour le moteur 
		float c = moteur.prixPlusTaux(id_voiture, prix);
		//Taxes totales
		float taxes = a + b + c;
		float prixFixeL = reser.prixFixeLoc(prix, nb_jour); // Je récupèrele prix fixe de la location
		float prixFinal = prixFixeL + taxes; // et je lui ajoute les 3 types de taxes pour obtenir le prix final de location
		return prixFinal;
	}

	/**
	 * Methode testant la disponnibilite du vehicule a la location
	 * @throws SQLException
	 */
	public void dispo_vehicule(Voiture voiture) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = "SELECT v.id_voiture \n FROM voiture AS v \n WHERE  v.reserve = 'dispo'";
		Statement statement = conn.createStatement();
		ResultSet resultat = statement.executeQuery(sql);
		System.out.println(statement.toString());
		while (resultat.next()) {
			System.out.println(resultat.getInt("id_voiture"));
		}
	}

	/**
	 * Methode bilan de la reservation 
	 * @param client
	 * @param id_voiture
	 * @throws SQLException
	 */
	public void bilanReservation(int agence_retour, int agence_depart,Reservation reservation,int id_reservation,Client client, int id_voiture) throws SQLException {
		VoitureManager vehiculemanag = new VoitureManager();
		int nb_jour = RechercheVoiture.duree;
		String date_depart = "" + RechercheVoiture.jourdeb + "/" + RechercheVoiture.moisdeb + "";
		String date_retour = "" + RechercheVoiture.jourfin + "/" + RechercheVoiture.moisfin + "";
		List<String> donnees_voitures = vehiculemanag.recupDonneesVoiture(id_voiture);
		String immatriculation = donnees_voitures.get(0);
		String marque = donnees_voitures.get(1);
		String modele = donnees_voitures.get(2);
		String transmission = donnees_voitures.get(3);
		int prix = Integer.parseInt(donnees_voitures.get(4));
		int id_type = Integer.parseInt(donnees_voitures.get(5));
		int id_puissance =  Integer.parseInt(donnees_voitures.get(6));
		int id_moteur =  Integer.parseInt(donnees_voitures.get(7));

		Voiture voiture = new Voiture(id_voiture);
		Type type = new Type(id_type);
		Puissance puissance = new Puissance(id_puissance);
		Moteur moteur = new Moteur(id_moteur);

		String message;
		if (agence_retour == agence_depart){
			message = "Nom : "+client.getNom()+"\n"+
					"Prenom : "+ client.getPrenom() +"\n"+
					"N client : "+ client.getId_client()+"\n"+
					"		---------------			\n"+
					"N de reservation : "+id_reservation+"\n"+
					"Date de debut : "+date_depart+"     "+"Date de fin : "+date_retour+"\n"+
					"Retour dans la meme agence ( pas de supplement )\n"+
					"\n"+
					"N de vehicule : " + voiture.getId_voiture()+"\n"+
					"Marque : "+ marque+"\n"+
					"Modele : "+ modele+"\n"+
					"Immatriculation : "+immatriculation+"\n"+
					"Transmission :"+transmission+"\n\n"+
					"Prix fixe du vehicule de base " + prixFixeLoc(prix, nb_jour)+"\n"+
					"Type : "+ voiture.typeVoiture(id_voiture).replaceAll("//s", "")+"  "+ " + "+type.prixPlusTaux(prix, id_voiture)+"\n"+
					"Puissance : "+ voiture.puissanceVoiture(id_voiture).replaceAll("//s", "")+ "  "+ " + "+
					puissance.prixPlusTaux(id_voiture, prix)+" \n"+
					"Moteur : "+ voiture.moteurVoiture(id_voiture).replaceAll("//s", "") + "  " + " + "+ moteur.prixPlusTaux(id_voiture, prix)+"\n"+
					"		------------		\n"+
					"Prix final (suite aux 3 caracteristiques (type, puissance, moteur) : " + prixFinal(prix,id_voiture,puissance, type, moteur, nb_jour);
		}
		else {
			message = "Nom : "+client.getNom()+"\n"+
					"Prenom : "+ client.getPrenom() +"\n"+
					"N client : "+ client.getId_client()+"\n"+
					"		---------------			\n"+
					"N de reservation : "+id_reservation+"\n"+
					"Date de debut : "+date_depart+"     "+"Date de fin : "+date_retour+"\n"+
					"Retour dans une autre agence ( supplement de 25 euros )\n"+
					"\n"+
					"N de vehicule : " + voiture.getId_voiture()+"\n"+
					"Marque : "+ marque+"\n"+
					"Modele : "+ modele+"\n"+
					"Immatriculation : "+immatriculation+"\n"+
					"Transmission :"+transmission+"\n\n"+
					"Prix fixe du vehicule de base     " + prixFixeLoc(prix, nb_jour)+" euros\n"+
					"Type : "+ voiture.typeVoiture(id_voiture).replaceAll("//s", "") + "   + "+type.prixPlusTaux(prix, id_voiture)+"\n"+
					"Puissance : "+ voiture.puissanceVoiture(id_voiture).replaceAll("//s", "") + "   + "+
					puissance.prixPlusTaux(id_voiture, prix)+"\n"+
					"Moteur : "+ voiture.moteurVoiture(id_voiture).replaceAll("//s", "")  +" + "+moteur.prixPlusTaux(id_voiture, prix)+"\n"+
					"		------------		\n"+
					"Prix final (suite aux 3 caracteristiques (type, puissance, moteur) : "+prixFinal(prix,id_voiture,puissance, type, moteur, nb_jour)+" euros";
		}
		JOptionPane.showMessageDialog(null, message,"Reservation",JOptionPane.INFORMATION_MESSAGE);

	}


	@Override
	/**
	 * Methode herite d'interaction avec le vendeur
	 * creation d'une reservation
	 * via le bouton creation d'une reservation
	 */
	public void actionPerformed(ActionEvent arg0) {
		String nom = RechercheLoc.nom;
		String prenom = RechercheLoc.prenom;
		int nb_jour = RechercheVoiture.duree;

		ClientManager recuperer = new ClientManager();
		List<String> list_donnee=null;
		try {
			list_donnee = recuperer.recupDonneClient(nom, prenom);
		} catch (SQLException e1) {
			System.err.println("On ne peut pas recuperer les donnees");
		}
		String idclient = list_donnee.get(7);
		int id_client = Integer.parseInt(idclient);
		Client client = new Client(id_client,nom, prenom, list_donnee.get(0) , list_donnee.get(8),  list_donnee.get(1),list_donnee.get(2), list_donnee.get(3), list_donnee.get(4), list_donnee.get(5), list_donnee.get(6) );

		String idvoiture = RechercheVoiture.id_voit;
		int id_voiture = Integer.parseInt(idvoiture);

		ReservationManager reserv = new ReservationManager();
		String date_depart = "" + RechercheVoiture.jourdeb + "/" + RechercheVoiture.moisdeb + "";
		String date_retour = "" + RechercheVoiture.jourfin + "/" + RechercheVoiture.moisfin + "";

		String agence_depart = JOptionPane.showInputDialog(null, "Agence de depart","Reservation",JOptionPane.QUESTION_MESSAGE);
		int repagence = JOptionPane.showConfirmDialog(null,"Allez-vous ramener la voiture dans l'agence de depart ?","Reservation",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		String agence_retour = null;
		if (repagence == JOptionPane.OK_OPTION) {
			agence_retour = agence_depart;
		}
		else{
			agence_retour = JOptionPane.showInputDialog(null, "Agence de retour","Reservation",JOptionPane.QUESTION_MESSAGE);
		}
		agencedep = Integer.parseInt(agence_depart);
		agenceret = Integer.parseInt(agence_retour);
		String nb_jours = Integer.toString(nb_jour);
		Reservation reservation = new Reservation(date_depart,nb_jours,date_retour,agencedep, agenceret);
		try {
			id_reservation = reserv.save(reservation, id_voiture, id_client);
			reserv.rendreNonDispo(id_voiture);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			reservation.bilanReservation(agenceret, agencedep, reservation, id_reservation,client, id_voiture);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int reponse = JOptionPane.showConfirmDialog(null,"Valider la reservation ?","Validation",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (reponse == JOptionPane.OK_OPTION) {
			JOptionPane.showMessageDialog(null,"Reservation confirmee." ,"Confirmation",JOptionPane.INFORMATION_MESSAGE);
			Application appli = new Application("connecte");
			this.application.dispose();

		}
		else{
			int reponse1 = JOptionPane.showConfirmDialog(null,"Reservation annulee. Voulez-vous recommencer ?" ,"Consulter fiche_client",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (reponse1 == JOptionPane.OK_OPTION) {
				Application appli = new Application("recherche");
				this.application.dispose();
			}
			else{
				JOptionPane.showMessageDialog(null,"Reservation confirmee." ,"Confirmation",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	public String getDate_depart() {
		return date_depart;
	}

	public String getDate_retour() {
		return date_retour;
	}
}
