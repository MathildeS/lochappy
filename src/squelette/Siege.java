package squelette;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import action.Application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Classe permettant au siege d'avoir acces a toutes les donnees des agences
 * @author groupe LocHappy
 *
 */
public class Siege extends Utilisateur{

	/**
	 * Attributs
	 */
	private List<Agence> agences;

	private List<Vendeur> vendeurs;

	/**
	 * Constructeur 1
	 * @param identifiant
	 * @param mot_de_passe
	 * @param connect
	 */
	public Siege(String identifiant, String mot_de_passe,String connect) {
		super(identifiant, mot_de_passe,connect);
	}

	/**
	 * Constructeur 2
	 */
	public Siege() {
	}

	/**
	 * Constructeur 3
	 * @param texte
	 * @param application
	 */
	public Siege(String texte, Application application) {
		super(texte, application);
		this.agences = new ArrayList<Agence>();
		this.vendeurs = new ArrayList<Vendeur>();
	}

	/**
	 * Methode pour calculer les statistiques
	 * et permettre au siege d'avoir acces a ces donnees
	 * @param agence
	 * @param vendeur
	 */
	public void statistiques(Agence agence, Vendeur vendeur) {
		// sur les types dans voiture
		// sur "quelle agence est la plus sollicit�e"
		int nb_reservation;
		System.out.println(agence.getReservations());	
	}

	/**
	 * Methode de fidelisation des clients
	 * encore un peu superficielle
	 */

	public void fidelite() { // Fidelite des clients

	}

	/**
	 * Methode d'acces aux donnees des agences
	 */
	public void accesdonnees_client() {
	}
	
	@Override
	/**
	 * Methode d'interaction du siege avec la BDD
	 */
	public void actionPerformed(ActionEvent e) {
		Application appli = new Application("Statistiques");
	}
}
