package squelette;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe de connexion � la BDD
 * @author groupe Lochappy
 *
 */
public class SingletonConnection {
	private final static String DB_URL = "jdbc:postgresql://localhost:5432/LocHappy";
	private final static String USER = "postgres";
	private final static String PASS = "postgres"; 
	
	private static Connection connection;
	
	private SingletonConnection() {
		try{
			Class.forName("org.postgresql.Driver"); 
			connection = DriverManager.getConnection(DB_URL, USER, PASS); // ouvre une connexion vers la base de donnees data
			System.out.println("Connexion etablie avec succes !");
		}
		catch(SQLException e){
			System.err.println("Impossible de creer la correction"); // pour gerer les erreurs (pas de pilote, base inexistante, etc.)
		}
		catch(ClassNotFoundException e) {
			System.err.println("Le driver n'est pas installe");
		}
	}
	
	public static Connection getInstance() {
		if(connection ==null ) {
			new SingletonConnection();	
		}
		return connection;
	}
}


