package squelette;

import java.awt.event.ActionEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import action.Application;

/**
 * Classe vendeur
 * @author groupe LocHappy
 *
 */
public class Vendeur extends Utilisateur  {

	/**
	 * Attribus du vendeur
	 */
	private String nom;

	private String prenom;

	private String adresse_mail;

	/**
	 * Constructeur 1
	 * @param nom
	 * @param prenom
	 * @param identifiant
	 * @param mot_de_passe
	 * @param adresse_mail
	 * @param texte
	 */
	public Vendeur(String nom, String prenom, String identifiant, String mot_de_passe,String adresse_mail,String texte) {
		super(identifiant, mot_de_passe,texte);
		this.getIdentifiant();
		this.getMot_de_passe();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse_mail = adresse_mail;
	}

	/**
	 * Constructeur 2
	 * @param texte
	 * @param application
	 */
	public Vendeur(String texte,Application application) {
		super(texte,application);
	}

	/**
	 * Methode permettant la creation de comptes vendeurs
	 * @param id_ag
	 * @throws SQLException
	 */
	private void saveBDD(int id_ag) throws SQLException  {


		// Insertion dans la BDD de donnees compte

		Connection conn = SingletonConnection.getInstance();
		String sql = "INSERT INTO  compte(nom, prenom, adresse_mail, identifiant, mot_de_passe,id_agence)  VALUES (?,?,?,?,?,?)";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setString(1, this.nom);
		state.setString(2, this.prenom);
		state.setString(3, this.adresse_mail);
		state.setString(4, this.getIdentifiant());
		state.setString(5, this.getMot_de_passe());
		state.setInt(6, id_ag);

		//execution
		state.execute();
		state.close();
	}

	/**
	 * Methode pour demande des donnees a entrer lors de la creation du compte 
	 * PARTIE INTERFACE
	 */
	public void actionPerformed(ActionEvent e) {
		String nom = JOptionPane.showInputDialog(null, "Nom","Creation de Compte",JOptionPane.QUESTION_MESSAGE);
		String prenom = JOptionPane.showInputDialog(null, "Prenom","Creation de Compte",JOptionPane.QUESTION_MESSAGE);
		String mail = JOptionPane.showInputDialog(null, "Adresse Mail","Creation de Compte",JOptionPane.QUESTION_MESSAGE);
		String identifiant = JOptionPane.showInputDialog(null, "Identifiant","Creation de Compte",JOptionPane.QUESTION_MESSAGE);
		String mot_de_passe = JOptionPane.showInputDialog(null, "Mot de Passe","Creation de Compte",JOptionPane.QUESTION_MESSAGE);
		String id_agence = JOptionPane.showInputDialog(null, "N de l'agence \n######## \n## Possibilites : ##\nMontpellier id_agence = 1 \nParis id_agence = 2 \nBordeaux id_agence = 3"+
		" \nLyon id_agence = 4 \n########","Creation de Compte",JOptionPane.QUESTION_MESSAGE);
		int id_ag = Integer.parseInt(id_agence);


		// creation de l'objet vendeur
		Vendeur numero = new Vendeur (nom, prenom, identifiant, mot_de_passe, mail,null);

		// test si le vendeur n'existe pas encore, alors on creer un compte
		// sinon, message d'erreur
		try {
			numero.saveBDD(id_ag);
			JOptionPane.showMessageDialog(null,  "Votre Compte a bien ete cree", "Creation de Compte", JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
}
