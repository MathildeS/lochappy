package action;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import squelette.Client;
import squelette.SingletonConnection;

/**
 * Classe recherche de locations, creation de fiche client et recherche de fiches clients
 * @author groupe LocHappy
 *
 */
public class RechercheLoc extends AbstractAction{

	/**
	 * Possibilites de reponse
	 */

	private String[] ex = {"oui","non"};

	/**
	 * Possibilites de reponse pour les dates de location
	 */

	private String[] jour = {"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	private String[] mois = {"01","02","03","04","05","06","07","08","09","10","11","12"};
	private String[] annee = {"1930","1931","1932","1933","1934","1935","1936","1937","1938","1939","1940","1941","1942","1943","1944","1945","1946","1947","1948","1949","1950","1951","1952","1953","1954","1955","1956","1957","1958","1959","1960","1961","1962","1963","1964","1965","1966","1967","1968","1969","1970","1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002"};

	/**
	 * Connexion interface
	 */

	private Application application;

	/**
	 * Initialisation de variables qui resteront statiques afin de ne pas les re-rentrer
	 */

	public static String nom;
	public static String prenom;
	public static String permis;

	/**
	 * Constructeur 1 
	 * @param texte
	 * @param application2
	 */

	public RechercheLoc (String texte,Application application2) {
		super(texte);
		this.application = application2;
	}

	/**
	 * Constructeur 2
	 * @param existence
	 * @param jour
	 * @param mois
	 * @param annee
	 */

	public RechercheLoc(String[] existence, String[] jour, String[] mois,  String[] annee ) {
		this.ex = existence;
		this.jour = jour;
		this.mois = mois;
		this.annee = annee;
	}

	/**
	 * Constructeur 3
	 */
	public RechercheLoc() {
	}

	/**
	 * Getters
	 */
	public String[] getMois() {
		return mois;
	}

	public String[] getAnnee() {
		return annee;
	}

	/**
	 * Methode d'interaction avec BDD pour consulter une fiche client
	 * ou creer une fiche client
	 * pour pouvoir par la suite realiser la recherche de location
	 */

	public void actionPerformed(ActionEvent e) {

		// RECHERCHE SI LE CLIENT A DEJA UNE FICHE
		String existence = (String)JOptionPane.showInputDialog(null, "Le client est-il deja enregistre ?","Client",JOptionPane.QUESTION_MESSAGE,null,ex,ex[0]);
		Client client = new Client(null,null,null,null,null,null,null,null,null,null);
		if (existence == "oui") {

			nom = JOptionPane.showInputDialog(null, "Nom","Client",JOptionPane.QUESTION_MESSAGE);
			prenom = JOptionPane.showInputDialog(null, "Prenom","Client",JOptionPane.QUESTION_MESSAGE);
			permis = JOptionPane.showInputDialog(null,"N Permis","Client",JOptionPane.QUESTION_MESSAGE);

			Connection conn = SingletonConnection.getInstance();
			Statement state;
			int nb = 0;
			try {
				state = conn.createStatement();
				ResultSet result = state.executeQuery("SELECT COUNT(*) FROM fiche_client WHERE nom = '" + nom + "' AND prenom = '" + prenom + "' AND permis = '" + permis + "'");
				result.next();
				nb = result.getInt("count");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null,  "Nous n'avons pas pu avoir acces a la fiche client.", "ERREUR", JOptionPane.INFORMATION_MESSAGE);;
				System.out.println(e1);
			}
			if (nb == 0) {
				JOptionPane.showMessageDialog(null,  "Ce client n'existe pas.", "Client", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				String existe = "Existe";
				JOptionPane.showMessageDialog(null,  "Ce client existe.", "Client", JOptionPane.INFORMATION_MESSAGE);
				this.application.dispose();
				Application lochappy = new Application ("recherche");
			}
		}

		// SINON, ON LUI CREER UNE FICHE
		else {
			String nom = JOptionPane.showInputDialog(null, "Nom","Client",JOptionPane.QUESTION_MESSAGE);
			String prenom = JOptionPane.showInputDialog(null, "Prenom","Client",JOptionPane.QUESTION_MESSAGE);
			String journ = (String)JOptionPane.showInputDialog(null, "Entrez votre jour de naissance","Client",JOptionPane.QUESTION_MESSAGE,null,this.jour,this.jour[0]);
			String moisn = (String)JOptionPane.showInputDialog(null, "Entrez votre mois de naissance","Client",JOptionPane.QUESTION_MESSAGE,null,this.mois,this.mois[0]);
			String anneen = (String)JOptionPane.showInputDialog(null, "Entrez votre annee de naissance","Client",JOptionPane.QUESTION_MESSAGE,null,this.annee,this.annee[0]);
			String date_naissance = journ + "/" + moisn + "/" + anneen;
			String permis = JOptionPane.showInputDialog(null,"Num Permis","Client",JOptionPane.QUESTION_MESSAGE);
			Boolean test = client.verification_permis(permis); 
			String tel = JOptionPane.showInputDialog(null,"N telephone","Client",JOptionPane.QUESTION_MESSAGE);
			while (tel.length()!=10) {
				JOptionPane.showMessageDialog(null,  "Ce n'est pas le bon format, retentez !", "Client", JOptionPane.INFORMATION_MESSAGE);
				tel = JOptionPane.showInputDialog(null,"N telephone","Client",JOptionPane.QUESTION_MESSAGE);
			}
			String mail = JOptionPane.showInputDialog(null,"Adresse Mail","Client",JOptionPane.QUESTION_MESSAGE);
			String adresse = JOptionPane.showInputDialog(null,"Adresse","Client",JOptionPane.QUESTION_MESSAGE);
			String code_postal = JOptionPane.showInputDialog(null,"Code Postal","Client",JOptionPane.QUESTION_MESSAGE);
			String ville = JOptionPane.showInputDialog(null,"Ville","Client",JOptionPane.QUESTION_MESSAGE);
			String pays = JOptionPane.showInputDialog(null,"Pays","Client",JOptionPane.QUESTION_MESSAGE);
			client.setNom(nom);
			client.setPrenom(prenom);
			client.setDate(date_naissance);
			client.setPermis(permis);
			client.setNtel(tel);
			client.setMail(mail);
			client.setAdresse(adresse);
			client.setCodeP(code_postal);
			client.setVille(ville);
			client.setPays(pays);

			try {
				client.saveBDD();
				JOptionPane.showMessageDialog(null,  "La fiche client de " + prenom + " " + nom + " a bien ete cree", "Client", JOptionPane.INFORMATION_MESSAGE);
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null,  "La fiche client ne peut pas se creer.", "ERREUR", JOptionPane.INFORMATION_MESSAGE);
				e1.printStackTrace();
			}

			this.application.dispose();
			Application lochappy = new Application ("recherche");
		}
	}
}
