package action;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;


import action.RechercheLoc;
import bdd.AgenceManager;
import bdd.ClientManager;
import bdd.EntretienManager;
import bdd.ReservationManager;
import parc_auto.Voiture;
import squelette.Agence;
import squelette.AgenceBis;
import squelette.Facture;
import squelette.Reservation;
import squelette.Siege;
import squelette.Utilisateur;
import squelette.Vendeur;

/**
 * Classe d'interface 
 * @author groupe LocHappy
 *
 */
public class Application extends JFrame{

	// CREATION DES BOUTONS

	/**
	 * Initialisation des differents boutons
	 * NB : leur fonctionnalite est note "  "
	 */
	private JButton boutonco = new JButton (new Utilisateur ("Se Connecter",this));
	
	private JButton boutoncc = new JButton (new Vendeur ("Creer un compte", this));
	
	private JButton boutonrecherche = new JButton (new RechercheLoc("Rechercher une location",this));

	private JButton boutonrecherche2 = new JButton (new RechercheLoc("Changer de client",this));

	private JButton boutonreservation= new JButton (new RechercheVoiture ("Choisir une voiture",this));

	private JButton boutonreserv = new JButton(new ReservationManager("Consulter une reservation",this));
	
	private JButton boutonficheclient = new JButton(new ClientManager("Consulter une fiche client",this));
	
	private JButton boutonvoiture = new JButton(new Voiture("Consulter une voiture",this));
	
	private JButton boutonagence = new JButton (new AgenceBis("Consulter une agence",this));
	
	private JButton boutonentretien = new JButton (new EntretienManager("Gerer les entretiens",this));
	
	private JButton boutonct = new JButton (new Facture ("Facture", this));
	
	private JButton boutoncreerreservation = new JButton (new Reservation("Creation d'une reservation",this));
	
	private JButton boutonstats = new JButton (new Siege("Statistiques",this));

	private JButton boutonrendement = new JButton (new Agence("Bilan rendement",this));
	
	private JButton boutonparcauto = new JButton (new AgenceManager("Bilan parc automobile de l'entreprise",this));

	/**
	 * Constructeur 1
	 * @param s
	 */
	
	public Application(String s){
		this.init(s);
	}

	/**
	 * Methode d'initialisation de la connexion l'appli
	 * accommpagne de la partie graphique
	 * @param s
	 */
	
	private void init (String s){

		// PARTIE GRAPHIQUE
		this.setTitle("LocHappy");
		this.setSize(600,800);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(6,1));

		// SI OUVERTURE DE L'APPLICATION ET L'UTILISATEUR N'EST PAS CONNECTE 
		// POSSIBILITES / SE CONNECTER OU / CREER UN COMPTE
		if (s=="ouverture" || s=="pas connecte") {
			this.setVisible(true);
			System.out.println("co");
			this.getContentPane().add(boutonco);
			this.getContentPane().add(boutoncc);
		}

		// UNE FOIS CONNECTE : POSSIBILITE DE
		//  * LANCER UNE RECHERCHE EN CREANT OU ENTRANT UNE FICHE CLIENT DEJA EXISTANTE
		//  * AVOIR ACCES A LA FACTURE AU RETOUR DU VEHICULE EN AGENCE
		//  * AVOIR ACCES A DES DONNEES SUR LES AGENCES
		//  * POUVOIR GERER LES ENTRETIENS
		if (s=="connecte") {
			this.setVisible(true);
			this.getContentPane().add(boutonrecherche);
			this.getContentPane().add(boutonct);
			this.getContentPane().add(boutonagence);
			this.getContentPane().add(boutonentretien);
		}
		
		
		// SE CONNECTER EN TANT QUE SIEGE ET AVOIR ACCES A D'AUTRES DONNEES
		if (s=="connecteAsSiege") {
			this.setVisible(true);
			this.getContentPane().add(boutonreserv);
			this.getContentPane().add(boutonficheclient);
			this.getContentPane().add(boutonvoiture);
			this.getContentPane().add(boutonagence);
			this.getContentPane().add(boutonstats);
		}
		
		if(s=="Statistiques") {
			this.setVisible(true);
			this.getContentPane().add(boutonrendement);
			this.getContentPane().add(boutonparcauto);
		}
		// RECHERCHER UN VEHICULE
		if (s=="recherche") {
			this.setVisible(true);
			this.getContentPane().add(boutonrecherche2);
			this.getContentPane().add(boutonreservation);
		}
		
		//CREATION D'UNE RESERVATION
		if (s=="reservation") {
			this.setVisible(true);
			this.getContentPane().add(boutoncreerreservation);
		}
		
	}
	

}

