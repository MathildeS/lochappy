package action;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import squelette.SingletonConnection;
import squelette.Utilisateur;

/**
 * Classe permettant la recherche d'un vehicule
 * dont les caracteristiques sont entrees par l'utilisateur
 * et sauvegardees pour pouvoir, par la suite reserver
 * 
 * @author groupe LocHappy
 *
 */
public class RechercheVoiture extends AbstractAction {

	/**
	 * Pour connexion interface
	 */
	private Application application;


	/**
	 * Tables string utilisables lors de l'entree de donnees
	 */
	private String[] ex = {"oui","non"};
	private String[] jour = {"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	private String[] mois = {"01","02","03","04","05","06","07","08","09","10","11","12"};
	private String[] km = {"100","200","300","400","500","600","700","800","900","1000","1200","1400","1600","1800"};
	private String[] prix = {"100","200","300","400","500","600","700","800","900","1000","1200","1400","1600","1800"};

	private String[] marque_l = {null,"Renault","Toyota","Audi","Peugeot","Skoda","Maserati","Seat","Jeep","Mini","RollsRoyce","Jaguar","Nissan","Honda","Lotus","Mercedes","Opel","Infiniti","Hyundai","Mazda"};
	private String[] puissance_l = {null,"1","2","3","4"};
	private String[] type_l = {null,"1","2","3","4","5","6","7","8","9","10"};
	private String[] moteur_l = {null,"1","2","3","4"};
	private String[] transmission_l = {null,"Automatique","Manuelle"};

	private ArrayList<String> mois31 = new ArrayList<String>();

	private String message = "id_voiture          marque          modele          transmission          puissance          moteur          type \n";

	/**
	 * Valeurs qui resteront statiques afin de ne pas avoir a les re-rentrer
	 */
	private String puis;
	private String mots;
	private String typs;

	public static String jourfin;
	public static String moisfin;
	public static String jourdeb;
	public static String moisdeb;
	public static int duree;
	public static String id_voit;

	/**
	 * Constructeur RechercheVoiture
	 * @param texte
	 * @param application
	 */
	public RechercheVoiture (String texte, Application application) {
		super(texte);
		this.application = application;
	}

	/**
	 * Methode d'interaction avec l'utilisateur
	 */
	public void actionPerformed (ActionEvent e) {
		mois31.add("01");
		mois31.add("03");
		mois31.add("05");
		mois31.add("07");
		mois31.add("08");
		mois31.add("10");
		mois31.add("12");

		jourdeb = (String)JOptionPane.showInputDialog(null, "Entrez le jour de debut de votre location","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.jour,this.jour[0]);
		moisdeb = (String)JOptionPane.showInputDialog(null, "Entrez le mois de debut de votre location","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.mois,this.mois[0]);
		String duree0 = JOptionPane.showInputDialog(null, "Entrez la duree de votre location","Recherche",JOptionPane.QUESTION_MESSAGE);
		String fourchette_km = (String)JOptionPane.showInputDialog(null,"Entrez le nombre maximal de km a parcourir", "Recherche",JOptionPane.QUESTION_MESSAGE,null,this.km,this.km[0]);
		String prix_max = (String)JOptionPane.showInputDialog(null,"Entrez votre prix maximal par jour","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.prix,this.prix[0]);
		int prix = Integer.parseInt(prix_max);
		// CALCULES POUR RECUPERER LA DATE DE RETOUR DU VEHICULE
		int jourd = Integer.parseInt(jourdeb);
		int moisd = Integer.parseInt(moisdeb);
		duree = Integer.parseInt(duree0);

		int jourf = jourd + duree;
		int moisf = moisd;
		while (jourf > 31) {
			if (mois31.contains(Integer.toString(moisf))){
				jourf = jourf - 31;
				moisf = moisf + 1;
			}
			else {
				while (jourf > 30) {
					jourf = jourf - 30;
					moisf = moisf + 1;
				}
			}
			if (jourf==31 && (mois31.contains(Integer.toString(moisf))==false)){
				jourf = jourf - 30;
				moisf = moisf + 1;
			}
		}
		jourfin = Integer.toString(jourf);
		moisfin = Integer.toString(moisf);

		// DEMANDE SI PLUS DE CRITERES SONT NECESSAIRES
		String precision = (String)JOptionPane.showInputDialog(null,"Voulez vous plus de criteres ?","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.ex,this.ex[0]);

		if (precision == "oui") {
			String marque = (String)JOptionPane.showInputDialog(null,"Entrez la marque souhaitee","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.marque_l,this.marque_l[0]);
			JOptionPane.showMessageDialog(null,  "Pour la puissance : \n 1 = 5 CV \n 2 = 6 CV \n 3 = 7 CV \n 4 = 37 CV", "Recherche", JOptionPane.INFORMATION_MESSAGE);
			String puissance = (String)JOptionPane.showInputDialog(null,"Entrez la puissance souhaitee","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.puissance_l,this.puissance_l[0]);
			JOptionPane.showMessageDialog(null,  "Pour le type : \n 1 = Familiale \n 2 = Berline \n 3 = Break \n 4 = Monospace \n 5 = Citadine \n 6 = Coupe \n 7 = Cabriolet \n 8 = Pick-up \n 9 = Crossover \n 10 = Fourgonnette", "Recherche", JOptionPane.INFORMATION_MESSAGE);
			String type = (String)JOptionPane.showInputDialog(null,"Entrez le type souhaite","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.type_l,this.type_l[0]);
			JOptionPane.showMessageDialog(null,  "Pour le moteur : \n 1 = diesel \n 2 = essence \n 3 = hybride \n 4 = electrique", "Recherche", JOptionPane.INFORMATION_MESSAGE);
			String moteur = (String)JOptionPane.showInputDialog(null,"Entrez le moteur souhaite","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.moteur_l,this.moteur_l[0]);
			String transmission = (String)JOptionPane.showInputDialog(null,"Entrez le type de transmission souhaitee","Recherche",JOptionPane.QUESTION_MESSAGE,null,this.transmission_l,this.transmission_l[0]);

			if (puissance == null) {
				puissance = "1,2,3,4";
			}
			if (marque == null) {
				marque = " 'Renault' , 'Toyota' , 'Peugeot' , 'Audi' , 'Skoda' , 'Maserati' , 'Seat' , 'Jeep' , 'Mini' , 'RollsRoyce' , 'Jaguar' , 'Nissan' , 'Honda' , 'Lotus' , 'Mercedes' , 'Opel' , 'Infiniti' , 'Hyundai' , 'Mazda' ";
			}
			if (type == null) {
				type = "1,2,3,4,5,6,7,8,9,10";
			}
			if (moteur == null) {
				moteur = "1,2,3,4";
			}
			if (transmission == null) {
				transmission = "'automatique','manuelle'";
			}

			Connection conn = SingletonConnection.getInstance();
			Statement state;
			try {
				state = conn.createStatement();
				ResultSet result = state.executeQuery("SELECT * FROM (SELECT * FROM voiture WHERE reserve = 'dispo' AND id_agence = '" + Utilisateur.id_agence + "' AND entretien = 'non' AND prix <" + prix + ")AS tab WHERE tab.marque IN (" + marque + ") AND tab.id_puissance IN (" + puissance + ") AND tab.id_type IN (" + type + ") AND tab.id_moteur IN (" + moteur + ") AND tab.transmission IN (" + transmission + ")");

				boolean exist = false;
				if(result.next()){
					exist = true;
				}
				// S'il y a un resultat
				if (exist == true){
					while (result.next()) {

						message += "      " + result.getInt("id_voiture") + "      ";
						if (result.getInt("id_voiture")<10) {
							message += " ";
						}
						String ma = result.getString("marque").replaceAll("\\s", "");
						while (ma.length()<14) {
							ma+= " ";
						}
						message += ma;
						String mo = result.getString("modele").replaceAll("\\s", "");
						while (mo.length()<14) {
							mo+= " ";
						}
						message += mo;
						String tr = result.getString("transmission").replaceAll("\\s", "");
						while (tr.length()<15) {
							tr+= " ";
						}
						message += tr;
						int pui = result.getInt("id_puissance");
						if (pui==1) {
							puis = "    5 CV    ";
						}
						if (pui==2) {
							puis = "    6 CV    ";
						}
						if (pui==3) {
							puis = "    7 CV    ";
						}
						if (pui==4) {
							puis = "    37 CV   ";
						}
						message += "   " + puis + "   ";
						int mot = result.getInt("id_moteur");
						if (mot==1) {
							mots = "Diesel    ";
						}
						if (mot==2) {
							mots = "Essence   ";
						}
						if (mot==3) {
							mots = "Hybride   ";
						}
						if (mot==4) {
							mots = "Electrique";
						}
						message += " " + mots + " ";
						int typ = result.getInt("id_type");
						if (typ == 1) {
							typs = "Familiale";
						}
						if (typ == 2) {
							typs = "Berline";
						}
						if (typ == 3) {
							typs = "Break";
						}
						if (typ == 4) {
							typs = "Monospace";
						}
						if (typ == 5) {
							typs = "Citadine";
						}
						if (typ == 6) {
							typs = "Coupe";
						}
						if (typ == 7) {
							typs = "Cabriolet";
						}
						if (typ == 8) {
							typs = "Pickup";
						}
						if (typ == 9) {
							typs = "Crossover";
						}
						if (typ == 10) {
							typs = "Fourgonnette";
						}
						message += " " + typs + " \n";
					}
					JOptionPane.showMessageDialog(null,  "Les voitures suivantes sont disponibles :", "Recherche", JOptionPane.INFORMATION_MESSAGE);
					String id_voit = JOptionPane.showInputDialog(null, message + "\n \n \n Entrez l'id de la voiture choisie : ", "Reservation",JOptionPane.QUESTION_MESSAGE);
					this.application.dispose();
					Application creation_contrat = new Application("contrat");
				}
				// S'il n'y a pas de resultats
				else {
					JOptionPane.showMessageDialog(null, "Aucune voiture ne correspond a  ces criteres", "Recherche",JOptionPane.INFORMATION_MESSAGE);
					this.application.dispose();
					Application nouvel_essai = new Application ("reservation");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		// SI LES CRITERES INITIAUX SUFFISENT --> ON RECHERCHE LES VEHICULES CORRESPONDANTS
		else {
			Connection conn = SingletonConnection.getInstance();
			Statement state;
			try {
				state = conn.createStatement();

				ResultSet result = state.executeQuery("SELECT * FROM voiture WHERE reserve = 'dispo' AND id_agence = '" + Utilisateur.id_agence + "' AND entretien = 'non' AND prix <" + prix);

				JOptionPane.showMessageDialog(null,  "Les voitures suivantes sont disponibles", "Reservation", JOptionPane.INFORMATION_MESSAGE);

				boolean exist = false;
				if(result.next()){
					exist = true;
				}

				// S'il y a un resultat
				if (exist == true){
					while (result.next()) {
						message += "  " + result.getInt("id_voiture") + "  ";
						message += result.getString("marque").replaceAll("\\s", "");
						message += " " + result.getString("modele").replaceAll("\\s", "") + " ";
						message += " " + result.getString("transmission").replaceAll("\\s", "") + " ";
						int pui = result.getInt("id_puissance");
						if (pui==1) {
							puis = "5 CV";
						}
						if (pui==2) {
							puis = "6 CV";
						}
						if (pui==3) {
							puis = "7 CV";
						}
						if (pui==4) {
							puis = "37 CV";
						}
						message += "   " + puis + "   ";
						int mot = result.getInt("id_moteur");
						if (mot==1) {
							mots = "Diesel";
						}
						if (mot==2) {
							mots = "Essence";
						}
						if (mot==3) {
							mots = "Hybride";
						}
						if (mot==4) {
							mots = "Electrique";
						}
						message += " " + mots + " ";
						int typ = result.getInt("id_type");
						if (typ == 1) {
							typs = "Familiale";
						}
						if (typ == 2) {
							typs = "Berline";
						}
						if (typ == 3) {
							typs = "Break";
						}
						if (typ == 4) {
							typs = "Monospace";
						}
						if (typ == 5) {
							typs = "Citadine";
						}
						if (typ == 6) {
							typs = "Coupe";
						}
						if (typ == 7) {
							typs = "Cabriolet";
						}
						if (typ == 8) {
							typs = "Pickup";
						}
						if (typ == 9) {
							typs = "Crossover";
						}
						if (typ == 10) {
							typs = "Fourgonnette";
						}
						message += " " + typs + " \n";
					}
					JOptionPane.showMessageDialog(null,  "Les voitures suivantes sont disponibles :", "Recherche", JOptionPane.INFORMATION_MESSAGE);
					id_voit = JOptionPane.showInputDialog(null, message + "\n \n \n Entrez l'id de la voiture choisie : ", "Reservation",JOptionPane.QUESTION_MESSAGE);
					this.application.dispose();
					Application creation_reseravtion = new Application("reservation");
				}
				// S'il n'y a pas de resultats
				else {
					JOptionPane.showMessageDialog(null, "Aucune voiture ne correspond a ces criteres", "Recherche",JOptionPane.INFORMATION_MESSAGE);
					this.application.dispose();
					Application nouvel_essai = new Application ("recherche");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, "La recherche n'a pas pu aboutir", "ERREUR",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}


	
