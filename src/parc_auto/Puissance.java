package parc_auto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import bdd.VoitureManager;
import squelette.SingletonConnection;

public class Puissance {
	
	/**
	 * Attributs
	 */
	private int id_puissance;
	
	private int puissance;
	
	/**
	 * Pour se connecter � BDD
	 */
	Connection conn;
	
	/**
	 * Constructeur1
	 * @param id_puissance
	 * @param puissance
	 */
	public Puissance( int id_puissance, int puissance) {
		this.id_puissance = id_puissance;
		this.id_puissance = puissance;
	}
	
	/**
	 * Constructeur 2
	 * @param id_puissance
	 */
	public Puissance(int id_puissance) {
		this.id_puissance = id_puissance;
	}

	/**
	 * Methode calcul des taux du aux differentes puissances du vehicule possible
	 * @param id_voiture
	 * @param prixdebase
	 * @return
	 * @throws SQLException
	 */
	public float prixPlusTaux(int id_voiture, int prixdebase) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql2 = "SELECT id_puissance FROM voiture WHERE id_voiture = ?";
		PreparedStatement statement = conn.prepareStatement(sql2);
		statement.setInt(1,id_voiture);
		ResultSet type = statement.executeQuery();
		if(type.next()) {
			id_puissance = type.getInt("id_puissance");
			System.out.println(id_puissance);
		}
		float tauxT = 0;
		float prix = 0;
		
		if (id_puissance==1) {
			tauxT = (float) 0.02;
			prix = (float) prixdebase*(1+tauxT);
		}
		else if(id_puissance == 2){
			tauxT = (float) 0.03;
			prix = (float) prixdebase*(1+tauxT);
		}
		else if(id_puissance == 3){
			tauxT = (float) 0.04;
			prix = (float) prixdebase*(1+tauxT);
		}
		else if(id_puissance == 4){
			tauxT = (float) 0.07;
			prix = (float) prixdebase*(1+tauxT);
		}
		return prix;
	}

	/**
	 * Methode permettant la creation d'une puissance dans la BDD
	 * @param puissance
	 * @throws SQLException
	 */
	public static void save(String puissance) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = "INSERT INTO moteur(moteur) VALUES (?)";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setString(1,puissance);
		state.execute();
		state.close();
	}
	
	/**
	 * TEST
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		// Creation d'une puissance

		Scanner scan = new Scanner(System.in);
		System.out.println("Puissance :");
		int puissance = scan.nextInt();
	
		Puissance puissanceP = new Puissance(puissance);

		//VoitureManager.save(puissance);
		//int id = VoitureManager.bringBackId(voiture);
		//System.err.println(id);
		scan.close();
	}
}
