package parc_auto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import squelette.SingletonConnection;

public class Entretien {

	/**
	 * Attributs d'un entretien
	 */
	private int id_entretien;

	private int id_voiture;

	private String date_entree;

	private String date_sortie;

	/**
	 * Getters
	 */
	public String getDate_entree() {
		return date_entree;
	}

	public String getDate_sortie() {
		return date_sortie;
	}

	public int getId_voiture() {
		return id_voiture;
	}

	public int getId_entretien() {
		return id_entretien;
	}

	/**
	 * Setters
	 */
	public void setDate_entree(String date_entree) {
		this.date_entree = date_entree;
	}
	public void setDate_sortie(String date_sortie) {
		this.date_sortie = date_sortie;
	}
	public void setId_voiture(int id_voiture) {
		this.id_voiture = id_voiture;
	}

	public void setId_entretien(int id_entretien) {
		this.id_entretien = id_entretien;
	}

	/**
	 * Constructeur 1 
	 * @param id_entretien
	 * @param date_entree
	 * @param date_sortie
	 */
	public Entretien( String date_entree, String date_sortie) {
		this.date_entree = date_entree;
		this.date_sortie = date_sortie;
	}

	/**
	 * Constructeur 2
	 * @param id_entretien
	 */
	public Entretien(int id_entretien) {
		this.id_entretien = id_entretien;
	}

	/**
	 * Methode demandant des informations sur l'entretien
	 * @param voiture
	 */
	public static void demandeInfoUtilisateur(Voiture voiture) {
		Scanner scan = new Scanner(System.in);
		System.out.println("La voiture est-elle en entretien ? : \n true / false " );
		String bool = scan.nextLine();

		if (!bool.equals("true")) {
			System.out.println("la voiture n'est pas en entretien");
			return;
		}
		System.out.println("Date d'entrée en entretien (FORMAT : JJ/MM/AAAA) : " );
		String date_entree = scan.nextLine();
		System.out.println("Date de sortie d'entretien (FORMAT : JJ/MM/AAAA) : " );
		String date_sortie = scan.nextLine();

		Entretien entretien = new Entretien(date_entree, date_sortie);
		voiture.setEntretien(entretien);
	}

	/**
	 * Methode
	 * @param id_entretien
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void EntretienBool(int id_entretien) throws SQLException, ParseException {

		Scanner scan = new Scanner(System.in);
		System.out.println("La voiture est-elle en entretien ? : \n true / false " );
		String bool = scan.nextLine();
		System.out.println("Date d'entrée en entretien (FORMAT : JJ/MM/AAAA) : " );
		String date_entree = scan.nextLine();
		System.out.println("Date de sortie d'entretien (FORMAT : JJ/MM/AAAA) : " );
		String date_sortie = scan.nextLine();

		if (bool.equals("true")) {

			//Mettre la voiture en entretien (plus disponible)		
			// INSERT INTO entretien
			Connection conn = SingletonConnection.getInstance();
			String sql = "INSERT INTO  entretien(id_voiture, date_entree, date_sortie)  VALUES (?,?,?)";
			PreparedStatement state = conn.prepareStatement(sql);
			state.setInt(1, this.id_voiture);
			state.setString(2, this.date_entree);
			state.setString(3, this.date_sortie);

			// j'affiche le SQL
			System.out.println(state.toString());

			//execution
			state.execute();
			state.close();

			// Je demande les dates d'entree et de sortie

			SimpleDateFormat dtE = new SimpleDateFormat("JJ/MM/AAAA"); 
			Date dateE = dtE.parse(date_entree); 
			SimpleDateFormat dt1 = new SimpleDateFormat("AAAA-MM-JJ");
			System.out.println(dt1.format(dateE));
			date_entree = "JJ/MM/AAAA"; 

			SimpleDateFormat dtS = new SimpleDateFormat("JJ/MM/AAAA"); 
			Date dateS = dtS.parse(date_sortie); 
			SimpleDateFormat dt2 = new SimpleDateFormat("AAAA-MM-JJ");
			System.out.println(dt2.format(dateS));
			date_sortie = "JJ/MM/AAAA"; 

		} else {

			// La voiture reste disponible dans la base de donnees
			// INSERT INTO entretien
			Connection conn = SingletonConnection.getInstance();
			String sql = "INSERT INTO  entretien(id_voiture, date_entree, date_sortie)  VALUES (?,?,?)";
			PreparedStatement state = conn.prepareStatement(sql);
			state.setInt(1, this.id_voiture);
			state.setString(2, this.date_entree);
			state.setString(3, this.date_sortie);

			// J'affiche le SQL
			System.out.println(state.toString());

			//Execution
			state.execute();
			state.close();
		}
	}	

	/**
	 * Methode creant la liste des vehicules en entretien
	 * @return liste des vehicules en entretien
	 * @throws SQLException
	 */
	public ArrayList<Integer> listVehiculeEntretien() throws SQLException {
		ArrayList<Integer> voitures_en_entretien = new ArrayList<Integer>();

		Connection conn = SingletonConnection.getInstance();

		String sql = "SELECT e.id_voiture \n  FROM voiture AS v, entretien AS e \n WHERE v.id_voiture=e.id_voiture";
		PreparedStatement statement = conn.prepareStatement(sql);
		ResultSet resultat = statement.executeQuery();

		while (resultat.next()) {
			voitures_en_entretien.add(resultat.getInt("id_voiture"));
		}
		return voitures_en_entretien;
	}

	/**
	 * Methode rendant ce vehicule non disponible s'il est en entretien
	 * @throws SQLException
	 */
	public void rendreNonDispo() throws SQLException {
		ArrayList<Integer> list = this.listVehiculeEntretien();
		for (int i=0; i<list.size(); i++) {
			Connection conn = SingletonConnection.getInstance();
			String sql = "UPDATE voiture SET entretien='oui' WHERE id_voiture="+"'"+list.get(i)+"'";
			PreparedStatement statement = conn.prepareStatement(sql);
			statement.executeUpdate();
		}
	}

	public void rendreDispo() throws SQLException {
		ArrayList<Integer> list = this.listVehiculeEntretien();
		for (int i=0; i<list.size(); i++) {
			Connection conn = SingletonConnection.getInstance();
			String sql = "UPDATE voiture SET entretien='non' WHERE id_voiture="+"'"+list.get(i)+"'";
			PreparedStatement statement = conn.prepareStatement(sql);
			statement.executeUpdate();	
		}
	}
}
