package parc_auto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import squelette.SingletonConnection;

public class Moteur {
	
	/**
	 * Attributs
	 */
	
	private int id_moteur;

	private String moteur;
	
	/**
	 * Attribut permettant la connexion � la BDD
	 */
	Connection conn;
	
	/**
	 * Constructeur1
	 * @param moteur
	 */
	public Moteur(int id_moteur,String moteur) {
		this.moteur = moteur;
		this.id_moteur = id_moteur;
	}
	
	/**
	 * Constructeur 2
	 * @param id_moteur
	 */
	public Moteur(int id_moteur) {
		this.id_moteur = id_moteur;
	}
	
	/**
	 * M�thode calcul prix location par jour avec le caract�ristique moteur
	 * @return prix de base de la voiture sans les suppléments
	 * @throws SQLException 
	 */
	public float prixPlusTaux(int id_voiture, int prixdebase) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql2 = "SELECT id_moteur FROM voiture WHERE id_voiture = ?";
		PreparedStatement statement =  conn.prepareStatement(sql2);
		statement.setInt(1,id_voiture);
		ResultSet moteur = statement.executeQuery();
		if(moteur.next()) {
			int id_moteur = moteur.getInt("id_moteur");
		}
		float tauxT = 0;
		float prix = 0;

		if (id_moteur==1) {
			tauxT = (float) 0.02;
			prix = (float) prixdebase*(1+tauxT); // une diesel consomme moins mais co�te cher � l'entretien
		}
		else if(id_moteur == 2){
			tauxT = (float) 0.01; 
			prix =  (float)prixdebase*(1+tauxT);
		}
		else if(id_moteur == 3){
			tauxT = (float) 0.0225; // une hybride co�te logiquement plus cher
			prix =(float)  prixdebase*(1+tauxT);
		}
		else if(id_moteur == 4){
			tauxT = (float) 0.015; // on offre un avantage � louer des �lectrique --> meilleur pour 
								// l'environnement
			prix = (float) prixdebase*(1+tauxT);
		}
		return prix ;
	}
}
