# LocHappy

*  Application de location de voitures

# Auteurs

*  Segaud Mathilde
*  Marcon Chloé
*  Jeannest Lucie

# Installation de LocHappy

## API nécessaires

* #### Librairie à installer.jar

 Vous la trouverez dans le dossier joint nécessaire à l'obtention de la facture en fichier Pdf dans la classe Facture du package 1
 
* #### postgresql-42.2.5.jar

Vous la trouverez également dans le dossier en .jar, elle est nécessaire aux discussions avec pgAdmin qui contient nos bases de données 

* #### Les installer

1.  Clic droit sur Project au dessus de la barre à outil 
2.  Propriétés
3.  Java Build Path
4.  Add external JARs
5.  Sélectionner les deux librairies dans le dossier LocHappy
6.  Apply and close 

# Motivation 
### Problèmes résolus par LocHappy

* #### Pour le vendeur :
 LocHappy vise à simplifier les locations grâce à des commandes interrogatrices dirigées et des enregistrements automatiques en base de données des caractéristiques nécessaires des clients et des voitures. L’automatisation des enregistrements de données permet au vendeur en agence d’accéder à une traçabilité de ses clients et véhicules, il  peut ainsi consulter des fiches clients et des fiches de réservations afin de constater  si un véhicule doit aller en entretien ou non. La mise en entretien est également gérée  par l’application, il suffit pour le vendeur de se connecter à son espace vendeur à  l’aide de ses identifiants et de remplir les données demandées sur la voiture concernée  pour la mettre en entretien.

* #### Pour le siège :
L'enregistrement de toutes les données sur une même base de données donne la possibilité au siège de consulter ses rendements et statistiques afin de savoir quelles agences sont les plus rentables, ou lesquelles ont besoin d'un renforcement d'effectif en véhicules.

* #### Pour le client :
Le client se rend en agence et créé sa fiche client une fois avec l'aide du vendeur. Il peut choisir sa voiture sur l'application selon ses critères et la réserver en 1 clic. Les caractéristiques de sa réservation sont enregistrées sur la base de données commune à toutes les agences LocHappy, ce qui lui permet de retourner sa voiture dans l'agence qu'il souhaite en renseignant simplement son nom, prénom et numéro de permis. Après avoir renseigné les dernières conditions de sa réservation, il reçoit sa facture et revient nous voir rapidement après constat de la sympathie de nox prix.

# Comment utiliser l'application

Ouvrir Eclipse 

* Ouvrir le projet récupéré précédemment
* (File, New, Java Project, Use default localisation (décoché), choisir la localisation du projet, Finish)

##### 1. Une connection est néessaire

Vous pouvez vous créer un compte vendeur en sélectionnant "Créer un compte" 
Ou vous connecter avec les identifants déjà réalisés tels que :

*  SHutte : LocSHutte2
*  Sbien : LOcHapSBi3      

Si vous souhaitez consulter ce que peut voir le siège :

* siege : mdp1234

##### 2. Une fois connecté, vous pouvez :

*  Rechercher une location
*  Obtenir une facture
*  Consulter une agence
*  Gerer les entretiens

Dans "Rechercher une location" LocHappy va vous demander si le client concerné est déjà connu dans ses services; vous pouvez soit vous créer une nouvelle fiche client en répondant que non, soit utiliser un de nos clients déjà créés :

* Pepper Maxence 125847800
* Vermeil Bastien 129678459

Si la connexion échoue LocHappy vous reproposera de rentrer vos identifiants, si elle réussi vous pourrez :

* Changer de client
* Choisir une voiture

Une fois que vous aurez renseigné vos critères de réservation, LocHappy vous proposera sa sélection de voitures correspondantes disponibles, il faudra sélectionner l'identifiant de celle que vous désirez et la renseigner en barre de saisie. Après sélection, LocHappy propose de créer une réservation, le client doit renseigner s'il va retourner sa voiture dans la même agence ou non et une fiche réservation sort récapitulant les caractéristiques de la réservation. Vous pouvez alors valider, ou non votre réservation. 

##### 3. Si vous validez la réservation le vendeur pourra avoir accès à la facture en fin de location en renseignant nom, prénom et numéro de permis du client concerné.

##### 4. Le vendeur peut également consulter ses agences en renseignant leur nom, il obtient alors leur effectif et les voitures les remplissant.

##### 5. Enfin il peut mettre ou retirer des voitures en entretien, et tout simplement consulter les entretiens en renseignant les numéros d'entretien et plaques d'immatriculation si nécessaire.

Si vous vous connectez en tant que siège vous pourrez :

* Consulter une réservation
* Consulter une fiche client
* Consulter une voiture
* Consulter une agence
* Consulter les statistiques de LocHappy














